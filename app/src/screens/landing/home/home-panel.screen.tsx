import BottomSheet, { BottomSheetView } from '@gorhom/bottom-sheet'
import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { observer } from 'mobx-react'
import React, { useMemo, useRef, useState } from 'react'
import { Dimensions, Image, NativeModules, Platform, Pressable, StyleSheet, TouchableOpacity, View } from 'react-native'
import AppText from '../../../components/AppText'
import { Colors } from '../../../constants/appColors'
import { DarkImages, Images } from '../../../constants/appImages'
import { bottomTabsHeight, deviceHeight } from '../../../constants/constants'
import { reactScreens } from '../../../constants/screensName'
import { useTheme } from '../../../contexts/ThemeContext'
import { BottomSheetIndexesEnum } from '../../../enums/bottom-sheet-index.enum'
import { LandingModesEnum } from '../../../enums/landing-modes.enum'
import { RootStackParamList } from '../../../models/root-stack-param-list'
import LandingDataService from '../../../services/landing-data.service'
import NavigationDataService from '../../../services/navigation-data.service'
import { getResolutionStyles } from '../../../utils/getResolutionStyles'
import SearchArea from './components/SearchArea'
const {
  SearchArea: { fontSize12 },
} = getResolutionStyles()
const communicateWithNative = NativeModules.CommunicateWithNative

export const getRecenterHeight = (parentViewHeight: number, snapPointPercent: string) => {
  const snapPointHeightReductionPercent = Platform.OS === 'android' ? 9 : 0
  // todo: recheck the height calculation logic in android side
  const snapPointRatio = (+snapPointPercent.slice(0, -1) - snapPointHeightReductionPercent) / 100
  return parentViewHeight * snapPointRatio
}

type Props = {
  route: RouteProp<RootStackParamList, 'Home'>
  navigation: StackNavigationProp<RootStackParamList, 'Home'>
}

export type TabType = {
  label: string
  key: LandingModesEnum
  icon: any
  activeIcon: any
  darkIcon: any
  darkActiveIcon: any
  analyticKey?: any
}

export const tabs: TabType[] = [
  {
    key: LandingModesEnum.NearbyAmenities,
    label: 'NearbyAmenities',
    icon: Images.tabParkingNew,
    darkIcon: DarkImages.tabParkingNew,
    activeIcon: Images.tabParkingNewSelected,
    darkActiveIcon: DarkImages.tabParkingNewSelected,
  },
  {
    key: LandingModesEnum.LiveCamera,
    label: 'LiveCamera',
    icon: Images.tabErpNew,
    darkIcon: DarkImages.tabErpNew,
    activeIcon: Images.tabErpNewSelected,
    darkActiveIcon: DarkImages.tabErpNewSelected,
  },
]

const { BottomSheet: bottomSheetValues } = getResolutionStyles()
const HomePanel: React.FC<Props> = ({ route, navigation }) => {
  const sheetRef = useRef<BottomSheet>(null)
  const snapPoints = bottomSheetValues.snapPoints
  const [parentViewHeight, setParentViewHeight] = useState(Dimensions.get('window').height)
  const { themeColors, themeImages, isDarkMode } = useTheme()

  // undefined is default value; for ios, manual calculation is done.
  const containerHeight = useMemo(() => (Platform.OS === 'ios' ? deviceHeight - bottomTabsHeight : undefined), [])

  const handleChangeBottomSheetIndex = (index: BottomSheetIndexesEnum) => {
    const checkSnapPoint = snapPoints
    LandingDataService.updateBottomSheetIndex(index)
    // last snap point is in full so we dont sent it
    if (index === BottomSheetIndexesEnum.MIN) {
      const reductionValue = Platform.OS === 'android' ? 65 : 0
      NavigationDataService.adjustRecenterNative(+checkSnapPoint[index] - reductionValue, index, reactScreens.Landing)
    } else if (index === 1) {
      const recenterHeight = getRecenterHeight(parentViewHeight, checkSnapPoint[1] as string)
      NavigationDataService.adjustRecenterNative(
        recenterHeight - (Platform.OS === 'android' ? 10 : 25),
        1,
        reactScreens.Landing,
      )
    }
  }

  const renderTabs = () => {
    return tabs.map((item: TabType) => {
      const isActive = LandingDataService.activeTab === item.key
      const imageSource = getIconSource(isActive, item)

      return (
        <View style={styles.tabContainer} key={item.key}>
          <Pressable
            style={[
              styles.tab,
              {
                backgroundColor: isActive ? themeColors.homeTabBackgroundActived : themeColors.vehicleIUNumber,
                borderColor: isActive ? themeColors.homeTabBackgroundActived : themeColors.homeTabBorder,
              },
            ]}
            onPress={() => handleChangeActiveTab(item.key)}>
            <Image source={imageSource} style={{ width: 25, height: 25, marginRight: 12 }} />
            <AppText
              style={{
                color: isActive ? Colors.white : themeColors.homeTabLabel,
                fontWeight: '500',
                fontSize: fontSize12,
              }}
              numberOfLines={2}>
              {item.label}
            </AppText>
          </Pressable>
          {isActive && !LandingDataService.isBottomSheetScrolled && LandingDataService.bottomSheetIndex > 0 && (
            <Image
              source={themeImages.homeActiveTabArrow}
              style={{ marginTop: -6, width: 12, height: 12 }}
              resizeMode={'contain'}
            />
          )}
        </View>
      )
    })
  }

  const getIconSource = (isActive: boolean, item: TabType) => {
    if (isActive) {
      if (isDarkMode) {
        return item.darkActiveIcon
      }
      return item.activeIcon
    }

    return isDarkMode ? item.darkIcon : item.icon
  }

  const handleChangeActiveTab = (selectedTab: LandingModesEnum) => {
    if (
      LandingDataService.activeTab !== selectedTab &&
      LandingDataService.bottomSheetIndex === BottomSheetIndexesEnum.MIN
    ) {
      LandingDataService.updateBottomSheetIndex(BottomSheetIndexesEnum.HALF)
    }

    LandingDataService.changeActiveTab(selectedTab)
  }

  return (
    <View
      style={styles.container}
      pointerEvents="box-none"
      onLayout={event => {
        setParentViewHeight(event.nativeEvent.layout.height)
      }}>
      <SearchArea />

      <BottomSheet
        index={LandingDataService.bottomSheetIndex}
        ref={sheetRef}
        backdropComponent={null}
        backgroundComponent={null}
        containerHeight={containerHeight}
        handleComponent={null}
        enableOverDrag={false}
        snapPoints={snapPoints}
        onChange={handleChangeBottomSheetIndex}>
        <BottomSheetView
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            marginTop: 0,
            // shadowColor: '#000',
            // shadowOffset: {
            //   width: 0,
            //   height: 2,
            // },
            // shadowOpacity: 0.23,
            // shadowRadius: 2.62,

            // elevation: 4,
          }}>
          <TouchableOpacity
            style={{
              marginBottom: 15,
              borderRadius: 50,
              backgroundColor: '#782EB1',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingVertical: 15,
              paddingHorizontal: 75,
              alignSelf: 'center',
            }}>
            <Image source={Images.startBroadcast} style={{ width: 44, height: 33 }} resizeMode="contain" />
            <AppText style={{ fontSize: 20, fontWeight: '400', marginLeft: 7, color: '#FFFFFF' }}>
              Start Broadcast
            </AppText>
          </TouchableOpacity>
          <View style={{ backgroundColor: 'white', flex: 1, paddingHorizontal: 24, paddingVertical: 14 }}>
            <View style={{ flexDirection: 'row' }}>{renderTabs()}</View>
          </View>
        </BottomSheetView>
      </BottomSheet>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
  },
  tabContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  tabs: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingTop: 12,
    paddingHorizontal: 24,
  },
  tab: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    borderWidth: 0.5,
    paddingHorizontal: 8,
    paddingVertical: 12,
    alignSelf: 'stretch',
    marginHorizontal: 4,
  },
})

export default observer(HomePanel)

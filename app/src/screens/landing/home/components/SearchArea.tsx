import { observer } from 'mobx-react'
import React, { useState } from 'react'
import { Image, Pressable, StyleSheet, View } from 'react-native'
import AppText from '../../../../components/AppText'
import { Colors } from '../../../../constants/appColors'
import { Images } from '../../../../constants/appImages'
import { useTheme } from '../../../../contexts/ThemeContext'
import LandingDataService from '../../../../services/landing-data.service'

type Props = {}
const SearchArea: React.FC<Props> = () => {
  const { themeColors, themeImages } = useTheme()
  const [searchAreaHeight, setSearchAreaHeight] = useState(0)

  const handleClearSearch = () => {
    LandingDataService.updateSearchValue('')
  }

  return (
    <View>
      <View
        style={[styles.searchArea, { backgroundColor: themeColors.primaryColor, marginTop: searchAreaHeight / 2 }]}
        onLayout={e => {
          setSearchAreaHeight(e.nativeEvent.layout.height)
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={[
              styles.searchSection,
              { backgroundColor: themeColors.inputBackground, paddingLeft: LandingDataService.searchValue ? 7 : 0 },
            ]}>
            <Pressable
              onPress={() => {
                console.log('click search button')
                // onOpenSearch?.()
              }}
              style={[styles.inputWrapper, { paddingLeft: LandingDataService.searchValue.length > 0 ? 16 : 8 }]}>
              {!LandingDataService.searchValue && (
                <Image
                  source={themeImages.searchInputIcon}
                  style={{
                    width: 22,
                    height: 22,
                  }}
                />
              )}
              <AppText style={[styles.searchInput, { marginLeft: LandingDataService.searchValue ? 0 : 4 }]}>
                {LandingDataService.searchValue ? LandingDataService.searchValue : 'Incident location'}
              </AppText>
            </Pressable>
            {LandingDataService.searchValue.length > 0 ? (
              <Pressable hitSlop={{ top: 20, bottom: 20, right: 20, left: 20 }} onPress={handleClearSearch}>
                <Image source={Images.clearTextInputIcon} />
              </Pressable>
            ) : (
              <View style={{ position: 'absolute', right: 0 }}>
                <Pressable style={styles.viewRoute}>
                  <Image source={Images.pinSearch} />
                </Pressable>
              </View>
            )}
          </View>
          {LandingDataService.searchValue.length > 0 && (
            <View>
              <Pressable style={styles.viewRoute}>
                <Image source={Images.pinSearch} />
              </Pressable>
            </View>
          )}
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  searchArea: {
    marginHorizontal: 24,
    borderRadius: 30,
    padding: 5,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    borderRadius: 30,
    paddingRight: 16,
  },
  searchIcon: {},
  searchInput: {
    // width: '100%',
    fontSize: 15,
    color: Colors.white,
    flex: 1,
  },

  inputWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  viewRoute: {
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    marginStart: 12,
    marginEnd: 4,
  },
  viewRouteLabel: {
    color: '#8432C3',
    fontSize: 16,
    fontWeight: '700',
  },

  btnTypeCarPark: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    padding: 8,
  },
  imgTypeCarPark: {
    width: 24,
    height: 24,
    marginRight: 8,
  },
  wrapTypeCarPark: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
    borderTopWidth: 0.5,
    borderColor: '#E2E2E2',
    marginTop: 8,
    paddingTop: 8,
  },
  txtTypeCarPark: {
    fontSize: 14,
    fontWeight: '500',
  },
  boxToolTipOBU: {
    flex: 1,
    backgroundColor: '#933DD8',
    borderRadius: 16,
    flexDirection: 'row',
    padding: 12,
    width: 250,
  },
  txtTitleToolTipOBU: { fontSize: 15, fontWeight: '500', marginBottom: 12, color: '#fff' },
  txtDesToolTipOBU: { fontSize: 15, fontWeight: '300', color: '#fff' },
  imgArrowDown: {
    width: 18,
    height: 13,
    alignSelf: 'flex-end',
    marginRight: 50,
    marginTop: -5,
    marginBottom: 10,
    tintColor: '#933DD8',
  },
  imgTabOBU: { alignSelf: 'flex-end', marginRight: 20, width: 79, height: 76 },
  txtPairing: { fontSize: 15, fontWeight: '400' },
  imgProgress: { height: 12, alignSelf: 'center', width: '100%', flex: 0.98 },
  boxProgress: { paddingHorizontal: 21, borderRadius: 16, paddingVertical: 9 },
  boxContentStatus: {
    paddingHorizontal: 24,
    paddingTop: 7,
  },
  boxClose: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icClose: {
    width: 16,
    height: 16,
  },
  btnClosePairing: {
    width: 25,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: -10,
  },
})

export default observer(SearchArea)

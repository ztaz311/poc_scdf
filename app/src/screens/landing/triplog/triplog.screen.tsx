import React, { memo, useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import TripLogService from '../../../services/triplog.service'

type Props = {}

const TripLogScreen: React.FC<Props> = () => {
  useEffect(() => {
    TripLogService.openTripLogNative()
  }, [])

  return <View style={styles.container} />
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default memo(TripLogScreen)

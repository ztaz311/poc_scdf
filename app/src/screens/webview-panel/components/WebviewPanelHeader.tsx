import React from 'react'
import { Pressable, StyleSheet, View } from 'react-native'
import { AppText } from '../../../components'
import { Colors } from '../../../constants/appColors'
import { useTheme } from '../../../contexts/ThemeContext'

type Props = {
  title: string
  onClose?: () => void
}

const WebviewPanelHeader: React.FC<Props> = ({ title, onClose }) => {
  const { themeColors } = useTheme()
  return (
    <View
      style={[
        styles.headerContainer,
        {
          backgroundColor: themeColors.primaryBackground,
        },
      ]}>
      <AppText
        style={[
          styles.headerTitle,
          {
            color: themeColors.primaryText,
          },
        ]}>
        {title}
      </AppText>
      <Pressable onPress={() => onClose?.()} style={styles.closeButtonContainer}>
        <AppText style={[styles.closeButton, { color: themeColors.primaryColor }]}>Close</AppText>
      </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: Colors.primaryBackground,
    paddingVertical: 20,
    paddingHorizontal: 18,
    borderBottomColor: '#999999',
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
    //debug border//
    borderWidth: 1,
  },
  headerTitle: {
    flex: 1,
    textAlign: 'left',
    fontSize: 18,
    fontWeight: '600',
  },
  closeButton: {
    fontSize: 16,
    fontWeight: '500',
    textAlign: 'right',
  },
  closeButtonContainer: {},
})

export default WebviewPanelHeader

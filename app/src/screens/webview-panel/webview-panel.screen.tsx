import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { observer } from 'mobx-react'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Dimensions, NativeModules, StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import WebView, { WebViewMessageEvent } from 'react-native-webview'
import { useTheme } from '../../contexts/ThemeContext'
import { RootStackParamList } from '../../models/root-stack-param-list'
import AuthService from '../../services/auth.service'
import { CustomWebviewEvent } from '../../services/inbox-messages.service'
import WebviewPanelHeader from './components/WebviewPanelHeader'
const CommunicateWithNative = NativeModules.CommunicateWithNative

const DEFAULT_RENDER_URL = 'https://event.breeze.com.sg/'
const DEFAULT_TITLE = 'Progress Tracking'

type WebviewPanelRouteProp = RouteProp<RootStackParamList, 'WebViewPanel'>
type WebviewPanelNavigationProp = StackNavigationProp<RootStackParamList, 'WebViewPanel'>
type Props = {
  route: WebviewPanelRouteProp
  navigation: WebviewPanelNavigationProp
}

const WebviewPanelScreen: React.FC<Props> = ({ route, navigation }) => {
  const WebviewDetails = useMemo(() => route.params, [route.params])
  const webviewUrl = useMemo(() => WebviewDetails.webviewUrl || DEFAULT_RENDER_URL, [WebviewDetails])
  const title = useMemo(() => WebviewDetails.title || DEFAULT_TITLE, [WebviewDetails])
  const { isDarkMode } = useTheme()

  // note: not sure if this state will be useful later on
  const [parentViewHeight, setParentViewHeight] = useState(Dimensions.get('window').height)

  useEffect(() => {
    AuthService.getIdToken().then(token => {
      setIdToken(token)
    })
  }, [])

  const handleMessageFromWebview = useCallback((event: WebViewMessageEvent) => {
    // todo?: maybe can even discard CustomWebviewEvent in favour of a more generic one
    const message: CustomWebviewEvent = event.nativeEvent?.data ? JSON.parse(event.nativeEvent.data) : null
    console.log('message from webview => ', message)
  }, [])
  const [idToken, setIdToken] = useState('')

  if (!idToken) return null

  return (
    <SafeAreaView
      style={[styles.container, { backgroundColor: isDarkMode ? '#222638' : '#fff' }]}
      pointerEvents="box-none"
      onLayout={event => {
        setParentViewHeight(event.nativeEvent.layout.height)
      }}>
      <WebviewPanelHeader
        title={title}
        onClose={() => {
          console.log('closing webview panel')
          CommunicateWithNative.closeProgressTrackingScreen()
        }}
      />
      <WebView
        onMessage={handleMessageFromWebview}
        onError={e => {
          console.log('webview panel; webview error: ', e)
        }}
        onHttpError={e => {
          console.log('webview panel; webview http error: ', e)
        }}
        originWhitelist={['*']}
        androidHardwareAccelerationDisabled={true}
        allowsFullscreenVideo={true}
        cacheEnabled={false}
        mediaPlaybackRequiresUserAction={false}
        source={{
          uri: webviewUrl,
          headers: {
            Authorization: `Bearer ${idToken}`,
            username: 'breeze-app',
          },
        }}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default observer(WebviewPanelScreen)

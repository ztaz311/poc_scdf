import AsyncStorage from '@react-native-async-storage/async-storage'
import { DefaultTheme, NavigationContainer } from '@react-navigation/native'
import { observer } from 'mobx-react'
import React, { useCallback, useState } from 'react'
import { Platform } from 'react-native'
import BottomTabs from '../components/Navigator/BottomTabs'
import { isNavigationReady, navigationRef } from '../components/Navigator/RootNavigation'
import { DARK_THEME_KEY } from '../constants/constants'
import { useTheme } from '../contexts/ThemeContext'
import { useShareBusinessLogic } from '../hooks/useNativeEvents'
import { ParamsFromNative } from '../services/navigation-data.service'
import ShareBusinessLogic from '../services/share-business-logic/share-business-logic.service'

type RouteProps = {
  initialRouteName?: string
  navigationParams?: ParamsFromNative
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#0000',
  },
}

const LandingRouter: React.FC<RouteProps> = () => {
  const { setTheme } = useTheme()
  const [isShowTutorial, setIsShowTutorial] = useState(false)

  const handleThemeChange = useCallback(
    (data: any) => {
      if (Platform.OS === 'ios') {
        AsyncStorage.setItem(DARK_THEME_KEY, String(data.isDarkMode), () => setTheme(data.isDarkMode))
      }
    },
    [setTheme],
  )

  const handleSkip = () => {
    setIsShowTutorial(false)
    // NavigationDataService.closeOnboardingTutorial()
  }

  useShareBusinessLogic(
    useCallback(
      ({ requestId, functionName, data }) => {
        switch (functionName) {
          case ShareBusinessLogic.constants.SEND_THEME_CHANGE_EVENT:
            handleThemeChange(data)
            break
          // case ShareBusinessLogic.constants.OPEN_ONBOARDING_TUTORIAL:
          //   setIsShowTutorial(true)
          //   break
        }
      },
      [handleThemeChange],
    ),
  )
  return (
    <NavigationContainer
      ref={navigationRef}
      theme={MyTheme}
      onReady={() => {
        isNavigationReady.current = true
      }}>
      <BottomTabs />
    </NavigationContainer>
  )
}

export default observer(LandingRouter)

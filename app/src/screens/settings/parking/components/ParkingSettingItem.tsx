import React from 'react'
import { Image, Pressable, StyleSheet, View } from 'react-native'
import { AppText } from '../../../../components'
import { Colors } from '../../../../constants/appColors'
import { useTheme } from '../../../../contexts/ThemeContext'
import { CarparkCountCode, CarparkDistanceCode } from '../../../../models/preferences-setting.model'

type Props = {
  value: string | number
  init: string
  isSelected: boolean
  isDefault: boolean
  code: CarparkCountCode | CarparkDistanceCode
  onSelectItem: (code: CarparkCountCode | CarparkDistanceCode) => void
}

const ParkingSettingItem: React.FC<Props> = ({ code, value, init, isSelected, isDefault, onSelectItem }) => {
  const { themeColors } = useTheme()
  return (
    <Pressable
      style={[
        styles.item,
        {
          backgroundColor: themeColors.primaryBackground,
          borderBottomColor: themeColors.borderBottomItemColor,
        },
      ]}
      onPress={() => onSelectItem(code)}>
      <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: themeColors.primaryBackground }}>
        <AppText style={[styles.textItem, { color: themeColors.textColorForDark }]}>{`${value}${init}`}</AppText>
        {isDefault && <AppText style={[styles.default, { color: themeColors.textColorForDark }]}> (Default)</AppText>}
      </View>
      {isSelected && <Image source={require('../../../../../assets/images/selected.png')} />}
    </Pressable>
  )
}

const styles = StyleSheet.create({
  item: {
    paddingVertical: 28,
    paddingHorizontal: 32,
    borderBottomColor: 'rgba(60, 60, 67, 0.360784)',
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  textItem: {
    fontSize: 16,
    fontWeight: '600',
    color: '#222638',
  },
  default: {
    color: '#778188',
    fontSize: 16,
    fontWeight: '500',
  },
})

export default ParkingSettingItem

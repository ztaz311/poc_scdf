import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { observer } from 'mobx-react'
import React, { useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { AppText } from '../../../components'
import BottomCover from '../../../components/BottomCover'
import ScreenHeader from '../../../components/ScreenHeader'
import { analyticEvents } from '../../../constants/analyticEvents'
import { useTheme } from '../../../contexts/ThemeContext'
import { RootStackParamList } from '../../../models/root-stack-param-list'
import NavigationDataService from '../../../services/navigation-data.service'
import PreferencesSettingsService from '../../../services/preferences-settings.service'
import PreferenceStyles from '../components/PreferenceStyles'
import ParkingSettingItem from './components/ParkingSettingItem'

type Props = {
  route: RouteProp<RootStackParamList, 'PetrolPreference'>
  navigation: StackNavigationProp<RootStackParamList, 'PetrolPreference'>
}

export const preferenceStyles = PreferenceStyles

const ParkingPreferenceScreen: React.FC<Props> = ({ navigation }) => {
  const { themeColors, themeImages } = useTheme()
  const handleBackClick = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.PreferencesParkingEvents.user_click.back,
      analyticEvents.PreferencesParkingEvents.screen_name,
    )
    navigation.goBack()
  }
  useEffect(() => {
    NavigationDataService.sendAnalyticsEventsScreenNameToNative(analyticEvents.PreferencesParkingEvents.screen_name)
  }, [])

  return (
    <SafeAreaView style={[preferenceStyles.container, { backgroundColor: themeColors.primaryBackground }]}>
      <BottomCover style={{ backgroundColor: themeColors.primaryBackground }} />
      <ScreenHeader
        onBack={handleBackClick}
        title="Parking"
        backIcon={themeImages.BackButton}
        backIconBgColor={themeColors.backIconBgColor}
      />
      <View
        style={[
          styles.titleWrapper,
          {
            backgroundColor: themeColors.primaryBackground,
            borderBottomColor: themeColors.borderBottomItemColor,
          },
        ]}>
        <AppText style={[styles.title, { color: themeColors.textColorForDark }]}>
          Choose the maximum number of carparks you wish to see around your current location and selected destination.
        </AppText>
      </View>
      {PreferencesSettingsService.carparkCountData?.map(item => {
        return (
          <ParkingSettingItem
            key={item.value}
            value={item.value}
            code={item.code}
            init=" carparks"
            isDefault={item.default === 'Y'}
            isSelected={PreferencesSettingsService.currentCarparCountSetting?.code === item.code}
            onSelectItem={code => {
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                `${item.value}_carparks`,
                analyticEvents.PreferencesParkingEvents.screen_name,
              )
              PreferencesSettingsService.updatePreferenceSetting('CarparkCount', code)
            }}
          />
        )
      })}
      <View
        style={[
          styles.titleWrapper,
          { backgroundColor: themeColors.primaryBackground, borderBottomColor: themeColors.borderBottomItemColor },
        ]}>
        <AppText style={[styles.title, { color: themeColors.textColorForDark }]}>
          Select the searching distance for carparks from your current location and selected destination.
        </AppText>
      </View>
      {PreferencesSettingsService.carparkDistanceData?.map(item => {
        return (
          <ParkingSettingItem
            key={item.value}
            value={item.value}
            code={item.code}
            init="m"
            isDefault={item.default === 'Y'}
            isSelected={PreferencesSettingsService.currentCarparDistanceSetting?.code === item.code}
            onSelectItem={code => {
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_click,
                `${item.value}m`,
                analyticEvents.PreferencesParkingEvents.screen_name,
              )
              PreferencesSettingsService.updatePreferenceSetting('CarparkDistance', code)
            }}
          />
        )
      })}
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  titleWrapper: {
    paddingVertical: 20,
    paddingHorizontal: 24,
    borderBottomColor: 'rgba(60, 60, 67, 0.360784)',
    borderBottomWidth: 1,
    backgroundColor: '#FCFCFC',
  },
  title: {
    fontSize: 16,
    color: '#778188',
  },
})

export default observer(ParkingPreferenceScreen)

import React from 'react'
import { Image, Pressable, StyleSheet } from 'react-native'
import { AppText } from '../../../../components'
import { Colors } from '../../../../constants/appColors'
import { Images } from '../../../../constants/appImages'
import { useTheme } from '../../../../contexts/ThemeContext'
import { MapDisplayCode, PreferenceSettingObject } from '../../../../models/preferences-setting.model'

type Props = {
  data: PreferenceSettingObject<MapDisplayCode>
  onItemPress?: (item: PreferenceSettingObject<MapDisplayCode>) => void
  isSelected?: boolean
}

const MapDisplaySettingItem: React.FC<Props> = ({ data, onItemPress, isSelected = false }) => {
  const { themeColors, themeImages } = useTheme()
  const imageSource = () => {
    switch (data.code) {
      case 'Light':
        return Images.mapDisplayLight
      case 'Dark':
        return Images.mapDisplayDark
    }
    return Images.mapDisplayAuto
  }
  return (
    <Pressable
      onPress={() => {
        onItemPress?.(data)
      }}
      style={[
        styles.container,
        {
          backgroundColor: themeColors.primaryBackground,
          borderBottomColor: themeColors.borderBottomItemColor,
        },
      ]}>
      <Image source={imageSource()} resizeMode={'contain'} style={styles.themeImage} />
      <AppText style={[styles.valueText, { color: themeColors.textColorForDark }]}>{data.value}</AppText>
      <Image
        source={Images.icTick}
        style={[
          styles.tickImage,
          {
            opacity: isSelected ? 1 : 0,
          },
        ]}
        resizeMode={'contain'}
      />
    </Pressable>
  )
}
const styles = StyleSheet.create({
  container: {
    borderBottomColor: Colors.divider,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingEnd: 40,
    paddingStart: 24,
    paddingVertical: 16,
  },
  tickImage: {
    tintColor: Colors.primaryColor,
    width: 16,
    height: 16,
  },
  valueText: {
    color: Colors.primaryText,
    fontSize: 16,
    flex: 1,
    paddingStart: 16,
  },
  themeImage: {
    width: 60,
    height: 60,
  },
})

export default MapDisplaySettingItem

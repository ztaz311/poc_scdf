import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { observer } from 'mobx-react-lite'
import React, { useCallback, useEffect } from 'react'
import { Platform, StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import BottomCover from '../../../components/BottomCover'
import ScreenHeader from '../../../components/ScreenHeader'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { useTheme } from '../../../contexts/ThemeContext'
import { useShareBusinessLogic } from '../../../hooks/useNativeEvents'
import { MapDisplayCode, PreferenceSettingObject } from '../../../models/preferences-setting.model'
import { RootStackParamList } from '../../../models/root-stack-param-list'
import NavigationDataService from '../../../services/navigation-data.service'
import PreferencesSettingsService from '../../../services/preferences-settings.service'
import ShareBusinessLogic from '../../../services/share-business-logic/share-business-logic.service'
import MapDisplaySettingItem from './components/MapDisplaySettingItem'

type Props = {
  route: RouteProp<RootStackParamList, 'MapDisplayPreference'>
  navigation: StackNavigationProp<RootStackParamList, 'MapDisplayPreference'>
}

const MapDisplayPreferenceScreen: React.FC<Props> = ({ route, navigation }) => {
  const { themeColors, themeImages, setTheme } = useTheme()
  const handleBackClick = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.PreferencesMapDisplayEvents.user_click.back,
      analyticEvents.PreferencesMapDisplayEvents.screen_name,
    )
    navigation.goBack()
  }
  useEffect(() => {
    NavigationDataService.sendAnalyticsEventsScreenNameToNative(analyticEvents.PreferencesMapDisplayEvents.screen_name)
  }, [])

  const onItemPressed = (item: PreferenceSettingObject<MapDisplayCode>) => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      item.value.toLowerCase().trim(),
      analyticEvents.PreferencesMapDisplayEvents.screen_name,
    )
    PreferencesSettingsService.updatePreferenceSetting('MapDisplay', item.code)
  }

  const handleThemeChanged = useCallback(
    (data: any) => {
      if (Platform.OS === 'ios') setTheme(data.isDarkMode)
    },
    [setTheme],
  )

  useShareBusinessLogic(
    useCallback(
      ({ functionName, data }) => {
        if (functionName === ShareBusinessLogic.constants.SEND_THEME_CHANGE_EVENT) {
          handleThemeChanged(data)
        }
      },
      [handleThemeChanged],
    ),
  )

  return (
    <SafeAreaView style={[styles.container, { backgroundColor: themeColors.primaryBackground }]}>
      <BottomCover style={{ backgroundColor: themeColors.primaryBackground }} />
      <ScreenHeader
        onBack={handleBackClick}
        title="Map display"
        backIcon={themeImages.BackButton}
        backIconBgColor={themeColors.backIconBgColor}
      />
      {PreferencesSettingsService.settingData?.MapDisplay.map((value, index) => {
        return (
          <MapDisplaySettingItem
            key={index}
            data={value}
            isSelected={PreferencesSettingsService.getMapDisplaySetting()?.code === value.code}
            onItemPress={onItemPressed}
          />
        )
      })}
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.primaryBackground,
  },
})

export default observer(MapDisplayPreferenceScreen)

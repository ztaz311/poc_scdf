import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { AppText } from '../../../components'
import BottomCover from '../../../components/BottomCover'
import ScreenHeader from '../../../components/ScreenHeader'
import Switch, { SwitchProps } from '../../../components/Switch'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { useTheme } from '../../../contexts/ThemeContext'
import { RootStackParamList } from '../../../models/root-stack-param-list'
import NavigationDataService from '../../../services/navigation-data.service'
import UserPreferenceService from '../../../services/user-preference-service'
import PreferenceStyles from '../components/PreferenceStyles'

type Props = {
  route: RouteProp<RootStackParamList, 'PetrolPreference'>
  navigation: StackNavigationProp<RootStackParamList, 'PetrolPreference'>
}

export const preferenceStyles = PreferenceStyles
type PetrolPrefObj = {
  is_selected: boolean
  element_id: string
  order: number
  element_name: string
  display_text: string
}
export const PetrolAmenityId = 'AP2'

const PetrolPreferenceScreen: React.FC<Props> = ({ navigation }) => {
  const [isNoPreference, setIsNoPreference] = useState(false)
  const [petrolPreferences, setPetrolPreferences] = useState<PetrolPrefObj[]>([])
  const { themeColors, themeImages } = useTheme()

  const switchConfigForPreferences: SwitchProps = {
    thumbColorActive: Colors.white,
    thumbColorInactive: themeColors.thumbColorInactive,
    trackColorActive: '#15B765',
    trackColorInactive: themeColors.trackColorInactive,
  }

  useEffect(() => {
    NavigationDataService.sendAnalyticsEventsScreenNameToNative(analyticEvents.PreferencesPetrolEvents.screen_name)
    const petrolAmenityObj = UserPreferenceService.userPreference?.amenities_preference.find(
      e => e.element_id === PetrolAmenityId,
    )
    const petrolSubItems: PetrolPrefObj[] =
      petrolAmenityObj?.sub_items?.map(e => ({
        is_selected: e.is_selected,
        element_id: e.element_id,
        element_name: e.element_name || 'NA',
        display_text: e.display_text || 'NA',
        order: e.order,
      })) || []
    setPetrolPreferences(petrolSubItems.sort((a, b) => a.order - b.order))

    const assumed_IsNoPreference = !!(petrolSubItems.every(e => !e.is_selected) && petrolAmenityObj?.is_selected)
    setIsNoPreference(assumed_IsNoPreference)
  }, [])

  const handlePrefToggle = async (value: boolean, prefId: string) => {
    const petrol = petrolPreferences.find(s => s.element_id === prefId)
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_toggle,
      `${petrol?.display_text.toLocaleUpperCase().trim().replace('-', '')}_${value ? 'on' : 'off'}`,
      analyticEvents.PreferencesPetrolEvents.screen_name,
    )
    const newPrefValues: PetrolPrefObj[] = petrolPreferences.map(petrolPref => {
      if (petrolPref.element_id === prefId)
        return {
          ...petrolPref,
          is_selected: value,
        }
      else return petrolPref
    })
    setPetrolPreferences(newPrefValues)
    if (isNoPreference && value) {
      setIsNoPreference(false)
    }

    const isPetrolAmenitySelected = newPrefValues.some(e => e.is_selected) || isNoPreference

    await UserPreferenceService.patchAmenityPrefAndSaveToLocal(
      PetrolAmenityId,
      [{ element_id: prefId, is_selected: value }],
      isPetrolAmenitySelected,
    )
  }
  const handleIsAllEnabledToggle = async (newIsNoPref: boolean) => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_toggle,
      newIsNoPref
        ? analyticEvents.PreferencesPetrolEvents.user_toggle.no_preference_on
        : analyticEvents.PreferencesPetrolEvents.user_toggle.no_preference_off,
      analyticEvents.PreferencesPetrolEvents.screen_name,
    )
    setIsNoPreference(newIsNoPref)
    let newPetrolPref = [...petrolPreferences]
    if (newIsNoPref) {
      newPetrolPref = petrolPreferences.map(ev => ({ ...ev, is_selected: false }))
      setPetrolPreferences(newPetrolPref)
    }
    const isAmenitySelected = newIsNoPref || newPetrolPref.some(e => e.is_selected)
    await UserPreferenceService.patchAmenityPrefAndSaveToLocal(PetrolAmenityId, newPetrolPref, isAmenitySelected)
  }

  // const handleBackClick = NavigationDataService.backToNative
  const handleBackClick = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.PreferencesPetrolEvents.user_click.back,
      analyticEvents.PreferencesPetrolEvents.screen_name,
    )
    navigation.goBack()
  }

  return (
    <SafeAreaView style={[preferenceStyles.container, { backgroundColor: themeColors.primaryBackground }]}>
      <BottomCover style={{ backgroundColor: themeColors.primaryBackground }} />
      <ScreenHeader
        onBack={handleBackClick}
        title="Petrol"
        backIcon={themeImages.BackButton}
        backIconBgColor={themeColors.backIconBgColor}
      />
      <View
        style={[
          preferenceStyles.preferenceSection,
          preferenceStyles.noPreference,
          {
            backgroundColor: themeColors.primaryBackground,
            borderBottomColor: themeColors.borderBottomItemColor,
          },
        ]}>
        <AppText style={[preferenceStyles.preferenceText, { color: themeColors.textColorForDark }]}>
          No preference
        </AppText>
        <Switch
          active={isNoPreference}
          containerStyle={preferenceStyles.switchStyle}
          onValueChange={handleIsAllEnabledToggle}
          {...switchConfigForPreferences}
        />
      </View>
      {petrolPreferences.map(e => (
        <View
          key={e.element_id}
          style={[
            PreferenceStyles.preferenceSection,
            PreferenceStyles.otherPreference,
            {
              backgroundColor: themeColors.primaryBackground,
              borderBottomColor: themeColors.borderBottomItemColor,
            },
          ]}>
          <AppText style={[PreferenceStyles.preferenceText, { color: themeColors.textColorForDark }]}>
            {e.display_text}
          </AppText>
          <Switch
            active={e.is_selected}
            onValueChange={value => handlePrefToggle(value, e.element_id)}
            containerStyle={PreferenceStyles.switchStyle}
            {...switchConfigForPreferences}
          />
        </View>
      ))}
    </SafeAreaView>
  )
}

export default PetrolPreferenceScreen

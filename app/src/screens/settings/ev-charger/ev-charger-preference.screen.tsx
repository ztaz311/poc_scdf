import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React, { useEffect, useState } from 'react'
import { Image, View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { AppText } from '../../../components'
import BottomCover from '../../../components/BottomCover'
import ScreenHeader from '../../../components/ScreenHeader'
import Switch, { SwitchProps } from '../../../components/Switch'
import { analyticEvents } from '../../../constants/analyticEvents'
import { Colors } from '../../../constants/appColors'
import { useTheme } from '../../../contexts/ThemeContext'
import { RootStackParamList } from '../../../models/root-stack-param-list'
import NavigationDataService from '../../../services/navigation-data.service'
import UserPreferenceService from '../../../services/user-preference-service'
import PreferenceStyles from '../components/PreferenceStyles'

export type EVPrefObj = {
  is_selected: boolean
  element_id: string
  element_name: string
  display_text: string
  order: number
}

export const EVAmenityId = 'AP5'

type Props = {
  route: RouteProp<RootStackParamList, 'EVChargerPreference'>
  navigation: StackNavigationProp<RootStackParamList, 'EVChargerPreference'>
}

const EVChargerPreferenceScreen: React.FC<Props> = ({ navigation }) => {
  const [isNoPreference, setIsNoPreference] = useState(false)
  const [evPreferences, setEvPreferences] = useState<EVPrefObj[]>([])
  const { isDarkMode, themeColors, themeImages } = useTheme()

  const switchConfigForPreferences: SwitchProps = {
    thumbColorActive: Colors.white,
    thumbColorInactive: themeColors.thumbColorInactive,
    trackColorActive: '#15B765',
    trackColorInactive: themeColors.trackColorInactive,
  }

  useEffect(() => {
    NavigationDataService.sendAnalyticsEventsScreenNameToNative(analyticEvents.PreferencesEVEvents.screen_name)

    const evAmenityObj = UserPreferenceService.userPreference?.amenities_preference.find(
      e => e.element_id === EVAmenityId,
    )
    const evSubItems: EVPrefObj[] =
      evAmenityObj?.sub_items?.map(e => ({
        is_selected: e.is_selected,
        element_id: e.element_id,
        order: e.order,
        element_name: e.element_name || 'NA',
        display_text: e.display_text || 'NA',
      })) || []
    setEvPreferences(evSubItems.sort((a, b) => a.order - b.order))
    const assumed_IsNoPreference = !!(evSubItems.every(e => !e.is_selected) && evAmenityObj?.is_selected)
    setIsNoPreference(assumed_IsNoPreference)
  }, [])

  const handlePrefToggle = async (value: boolean, prefId: string) => {
    const newPrefValues: EVPrefObj[] = evPreferences.map(evPref => {
      if (evPref.element_id === prefId)
        return {
          ...evPref,
          is_selected: value,
        }
      else return evPref
    })
    setEvPreferences(newPrefValues)
    if (isNoPreference && value) {
      setIsNoPreference(false)
    }
    const isEVAmenitySelected = newPrefValues.some(e => e.is_selected) || isNoPreference

    UserPreferenceService.patchAmenityPrefAndSaveToLocal(
      EVAmenityId,
      [{ element_id: prefId, is_selected: value }],
      isEVAmenitySelected,
    )
  }

  const handleIsAllEnabledToggle = async (newIsNoPref: boolean) => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_toggle,
      analyticEvents.PreferencesEVEvents.user_toggle.no_preference.replace('$value', isNoPreference ? 'on' : 'off'),
      analyticEvents.PreferencesEVEvents.screen_name,
    )
    setIsNoPreference(newIsNoPref)
    let newEvPref = [...evPreferences]
    if (newIsNoPref) {
      newEvPref = evPreferences.map(ev => ({ ...ev, is_selected: false }))
      setEvPreferences(newEvPref)
    }
    const isAmenitySelected = newIsNoPref || newEvPref.some(e => e.is_selected)
    await UserPreferenceService.patchAmenityPrefAndSaveToLocal(EVAmenityId, newEvPref, isAmenitySelected)
  }

  const handleBackClick = () => {
    NavigationDataService.sendAnalyticsEventsToNative(
      analyticEvents.user_click,
      analyticEvents.PreferencesEVEvents.screen_name,
      analyticEvents.PreferencesEVEvents.user_click.back,
    )
    navigation.goBack()
  }

  return (
    <SafeAreaView style={[styles.container, { backgroundColor: themeColors.primaryBackground }]}>
      <BottomCover style={{ backgroundColor: themeColors.primaryBackground }} />
      <ScreenHeader
        onBack={handleBackClick}
        title="EV"
        backIcon={themeImages.BackButton}
        backIconBgColor={themeColors.backIconBgColor}
      />

      <View
        style={[
          styles.preferenceSection,
          styles.noPreference,
          {
            backgroundColor: themeColors.primaryBackground,
            borderBottomColor: themeColors.borderBottomItemColor,
          },
        ]}>
        <AppText style={[styles.preferenceText, { color: themeColors.textColorForDark }]}>No preference</AppText>
        <Switch
          active={isNoPreference}
          containerStyle={styles.switchStyle}
          onValueChange={handleIsAllEnabledToggle}
          {...switchConfigForPreferences}
        />
      </View>
      {evPreferences.map(ev => (
        <View
          key={ev.element_id}
          style={[
            styles.preferenceSection,
            styles.otherPreference,
            {
              backgroundColor: themeColors.primaryBackground,
              borderBottomColor: themeColors.borderBottomItemColor,
            },
          ]}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={(themeImages.evChargerIcons as any)[ev.element_id]} style={{ marginRight: 5 }} />
            <AppText style={[styles.preferenceText, { color: themeColors.textColorForDark }]}>
              {ev.display_text}
            </AppText>
          </View>
          <Switch
            active={ev.is_selected}
            onValueChange={value => {
              NavigationDataService.sendAnalyticsEventsToNative(
                analyticEvents.user_toggle,
                `${ev.display_text.toLowerCase().trim().replace('-', '')}_${value ? 'on' : 'off'}`,
                analyticEvents.PreferencesEVEvents.screen_name,
              )
              handlePrefToggle(value, ev.element_id)
            }}
            containerStyle={styles.switchStyle}
            {...switchConfigForPreferences}
          />
        </View>
      ))}
    </SafeAreaView>
  )
}
const styles = PreferenceStyles

export default EVChargerPreferenceScreen

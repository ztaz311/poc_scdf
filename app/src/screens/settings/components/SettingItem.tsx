import React from 'react'
import { Image, ImageSourcePropType, ImageStyle, Pressable, StyleSheet, TextStyle } from 'react-native'
import { AppText } from '../../../components'
import { Colors } from '../../../constants/appColors'
import { Images } from '../../../constants/appImages'
import { useTheme } from '../../../contexts/ThemeContext'

type Props = {
  icon: ImageSourcePropType
  iconStyle?: ImageStyle
  title: string
  titleStyle?: TextStyle
  value?: string
  valueStyle?: TextStyle
  showRightArrow?: boolean
  onPress?: () => void
}

const SettingItem: React.FC<Props> = ({
  icon,
  iconStyle,
  title,
  titleStyle,
  value,
  valueStyle,
  showRightArrow = true,
  onPress,
}) => {
  const { themeColors } = useTheme()
  return (
    <Pressable style={[styles.container, { borderBottomColor: themeColors.divider }]} onPress={onPress}>
      <Image
        source={icon}
        style={[
          {
            width: 30,
            height: 30,
            alignContent: 'center',
            alignItems: 'center',
          },
          iconStyle,
        ]}
        resizeMode="contain"
      />
      <AppText numberOfLines={1} style={[styles.titleText, titleStyle]}>
        {title}
      </AppText>
      <AppText numberOfLines={1} style={[styles.valueText, valueStyle]}>
        {value}
      </AppText>
      {showRightArrow && (
        <Image
          source={Images.arrowRight}
          style={{
            marginStart: 10,
            width: 11,
            height: 11,
          }}
          resizeMode={'contain'}
        />
      )}
    </Pressable>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 24,
    alignItems: 'center',
    paddingVertical: 14,
    borderBottomWidth: 1,
  },
  titleText: {
    flex: 1,
    marginStart: 12,
    color: Colors.primaryText,
    fontWeight: '400',
    fontSize: 16,
  },
  valueText: {
    color: Colors.greyText,
    fontWeight: '400',
    fontSize: 16,
  },
})
export default SettingItem

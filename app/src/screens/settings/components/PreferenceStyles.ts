import { StyleSheet } from 'react-native'
import { Colors } from '../../../constants/appColors'

const PreferenceStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.primaryBackground,
  },
  preferenceText: {
    color: '#000000',
    fontWeight: '700',
    fontSize: 16,
  },
  switchStyle: {
    marginLeft: 13,
  },
  noPreference: {
    justifyContent: 'flex-end',
    // borderTopWidth: 1,
    backgroundColor: Colors.primaryBackground,
  },
  preferenceSection: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    borderColor: 'rgba(60, 60, 67, 0.360784)',
    borderBottomWidth: 1,
    paddingHorizontal: 24,
    paddingVertical: 27,
    backgroundColor: Colors.white,
  },
  otherPreference: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
})

export default PreferenceStyles

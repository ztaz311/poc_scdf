import { NativeEventEmitter, NativeModules, Platform } from 'react-native'

class ContactEvent extends NativeEventEmitter {
  openETAContactsPage: any

  constructor(nativeModule: any) {
    super(nativeModule)

    // explicitly set our custom methods and properties
    this.openETAContactsPage = Platform.OS === 'ios' ? nativeModule.openETAContactsPage : undefined
  }
}

export default new ContactEvent(NativeModules.ContactEvent)

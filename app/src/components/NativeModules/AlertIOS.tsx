import { AlertButton, NativeModules } from 'react-native'
import NavigationDataService from '../../services/navigation-data.service'

const AlertIOSModule = NativeModules.AlertIOS

const AlertIOS = {
  alert(title: string, message?: string, buttons: AlertButton[] = []) {
    const rootTag = NavigationDataService.rootTag
    const onButtonSelected = (buttonIndex: number) => {
      const button = buttons[buttonIndex]
      if (button.onPress) {
        button.onPress()
      }
    }
    AlertIOSModule.alert(rootTag, title, message, buttons, onButtonSelected)
  },
}

export default AlertIOS

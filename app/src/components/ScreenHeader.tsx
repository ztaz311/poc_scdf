import React from 'react'
import { Image, ImageSourcePropType, Pressable, StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
import { useTheme } from '../contexts/ThemeContext'
import AppText from './AppText'

type Props = {
  onBack?: () => void
  title?: string
  description?: string
  backIcon?: ImageSourcePropType
  backIconBgColor?: string
  hasBackButton?: boolean
  isRounded?: boolean
  isSmall?: boolean
  showShadow?: boolean
  textBtnRight?: string
  actionBtnRight?: () => void
  disabledBtnRight?: boolean
  styleBtnRight?: TextStyle
  containerStyle?: ViewStyle
  showDashedLine?: boolean
  onTitleClick?: () => void
  titleStyle?: TextStyle
  rightTextStyle?: TextStyle
}

const ScreenHeader: React.FC<Props> = ({
  onBack,
  title,
  description,
  hasBackButton = true,
  isRounded = false,
  isSmall = false,
  showShadow = true,
  textBtnRight = 'Done',
  actionBtnRight,
  disabledBtnRight = false,
  styleBtnRight = {},
  containerStyle = {},
  showDashedLine = false,
  onTitleClick,
  titleStyle = {},
}) => {
  const { themeColors, themeImages } = useTheme()

  const roundedStyle = isRounded ? styles.roundTop : {}

  const showShadowStyle = showShadow ? styles.shadow : {}

  const showBorderBottom = showDashedLine ? styles.dashedLine : {}

  return (
    <View style={showShadow ? styles.boxHeader : {}}>
      <View
        style={[
          styles.headerContainer,
          roundedStyle,
          {
            backgroundColor: themeColors.primaryBackground,
          },
          showShadowStyle,
          containerStyle,
          showBorderBottom,
        ]}>
        {hasBackButton && (
          <View style={[styles.backButtonContainer]}>
            <Pressable onPress={onBack} style={styles.backButtonPressable}>
              <Image source={themeImages.BackButton} resizeMode="center" />
            </Pressable>
          </View>
        )}
        <Pressable style={styles.headerTitleContainer} onPress={onTitleClick}>
          <AppText
            style={[styles.headerTitle, { color: themeColors.primaryText, fontSize: isSmall ? 18 : 22 }, titleStyle]}>
            {title}
          </AppText>
          {!!description && (
            <AppText style={[styles.headerTitleDesc, { color: themeColors.secondaryText }]}>{description}</AppText>
          )}
        </Pressable>
        {actionBtnRight && (
          <View style={[styles.rightButtonContainer]}>
            <Pressable onPress={actionBtnRight} style={styles.backButtonPressable} disabled={disabledBtnRight}>
              <AppText style={[styles.closeTxt, { color: themeColors?.primaryColor }, styleBtnRight]}>
                {textBtnRight}
              </AppText>
            </Pressable>
          </View>
        )}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  boxHeader: {
    overflow: 'hidden',
    paddingBottom: 5,
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  backButtonContainer: {
    position: 'absolute',
    left: 20,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10, // note: somehow this was needed to make the back button click work
  },
  rightButtonContainer: {
    position: 'absolute',
    right: 20,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10, // note: somehow this was needed to make the back button click work
  },
  backButtonPressable: {
    width: '100%',
    height: '100%',
    borderRadius: 20, // 50%
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  headerTitleContainer: {
    flex: 0.55,
    minHeight: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  headerTitle: {
    textAlign: 'center',
    fontWeight: '500',
  },
  headerTitleDesc: {
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 16,
    fontStyle: 'italic',
  },
  closeTxt: {
    fontWeight: '500',
    fontSize: 16,
  },
  roundTop: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },
  shadow: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowColor: '#000',
    shadowRadius: 3,
    elevation: 5,
  },
  dashedLine: {
    borderBottomColor: '#C1C1C1',
    borderBottomWidth: 1,
  },
})

export default ScreenHeader

import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react'
import { AlertButton, Modal, StyleSheet, TouchableOpacity, View } from 'react-native'
import { useTheme } from '../contexts/ThemeContext'
import AppText from './AppText'

export type CustomAlert = {
  show: (title: string, message: string | undefined, buttons: AlertButton[]) => void
}

const AlertAndroidComponent: React.ForwardRefRenderFunction<CustomAlert> = (_props, ref) => {
  const [show, setShow] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const [buttons, setButtons] = useState<AlertButton[]>([])
  const { themeColors } = useTheme()
  useImperativeHandle(ref, () => ({
    show: (titleTxt: string, mes, btns: AlertButton[]) => {
      setTitle(titleTxt)
      setMessage(mes || '')
      setButtons(btns)
      setShow(true)
    },
  }))

  useEffect(() => {
    if (!show) {
      setTitle('')
      setMessage('')
      setButtons([])
    }
  }, [show])

  if (!show) {
    return null
  }

  const onPressWrapper = (onPress?: () => void) => () => {
    setShow(false)
    if (onPress) {
      onPress()
    }
  }
  return (
    <Modal animationType="fade" transparent visible={show}>
      <View style={styles.rootContainer}>
        <View style={[styles.alert, { backgroundColor: themeColors.homeTabBackground }]}>
          {title !== '' && <AppText style={[styles.title, { color: themeColors.primaryText }]}>{title}</AppText>}
          {message ? <AppText style={[styles.message, { color: themeColors.primaryText }]}>{message}</AppText> : null}
          <View style={styles.buttonList}>
            {buttons.map((button, index) => {
              return (
                <TouchableOpacity key={index.toString()} style={styles.button} onPress={onPressWrapper(button.onPress)}>
                  <AppText
                    style={[
                      styles.buttonTextDefault,
                      button.style === 'destructive' && styles.textDestructive,
                      button.style === 'cancel' && { ...styles.textCancel, color: themeColors.primaryColor },
                    ]}>
                    {button.text}
                  </AppText>
                </TouchableOpacity>
              )
            })}
          </View>
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  rootContainer: {
    backgroundColor: 'rgba(1 , 1, 1, 0.5)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  alert: {
    width: '100%',
    padding: 20,
    borderRadius: 3,
    backgroundColor: 'white',
    elevation: 2,
  },
  title: {
    fontSize: 18,
    fontWeight: '400',
  },
  message: {
    fontSize: 16,
  },
  buttonList: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  button: { marginLeft: 20 },
  buttonTextDefault: {
    fontSize: 16,
    fontWeight: '600',
    color: '#0287c3',
  },
  textCancel: {
    color: 'gray',
  },
  textDestructive: {
    color: '#D63B2F',
  },
})

export default React.memo(forwardRef(AlertAndroidComponent))

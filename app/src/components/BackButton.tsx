import React from 'react'
import { Image, StyleProp, StyleSheet, View, ViewStyle } from 'react-native'
import { useTheme } from '../contexts/ThemeContext'
import TouchableComponent from './TouchableComponent'

type Props = {
  style?: StyleProp<ViewStyle>
  onPress: () => void
}

const BackButton: React.FC<Props> = ({ style, onPress }) => {
  const { themeImages } = useTheme()
  return (
    <TouchableComponent activeOpacity={0.6} onPress={onPress}>
      <View style={[styles.button, style]}>
        <Image source={themeImages.BackButton} style={styles.backImageStyle} />
      </View>
    </TouchableComponent>
  )
}

const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  backImageStyle: {
    resizeMode: 'stretch',
    padding: 0,
  },
})

export default BackButton

import React from 'react'
import { Platform, StyleProp, Text, TextProps, TextStyle } from 'react-native'
import { appTextStyles } from '../constants/appTextStyles'

const fontWeightTransformAndroid = (styles: StyleProp<TextStyle>[]): StyleProp<TextStyle> => {
  // iOS no need
  if (Platform.OS === 'ios') {
    return styles
  }

  // Get final fontWeight and fontFamily
  let fontWeight
  let fontFamily
  for (let i = styles.length - 1; i >= 0; i--) {
    const currentStyle = styles[i] as TextStyle
    if (!fontFamily && currentStyle?.fontFamily) {
      fontFamily = currentStyle?.fontFamily
    }
    if (!fontWeight && currentStyle?.fontWeight) {
      fontWeight = currentStyle?.fontWeight
    }
    if (fontFamily && fontWeight) {
      break
    }
  }
  // Get fontFamily based on fontWeight
  switch (fontWeight) {
    case '100':
    case '200':
    case '300':
      fontFamily += '-Light'
      break
    case '400':
    case 'normal':
      fontFamily += '-Regular'
      break
    case '500':
      fontFamily += '-Medium'
      break
    case '600':
      fontFamily += '-Semibold'
      break
    case '700':
    case '800':
    case '900':
    case 'bold':
      fontFamily += '-Bold'
      break
    default:
      fontFamily += '-Regular'
      break
  }
  const fontStyle: TextStyle = {
    fontFamily,
    // fontWeight: 'normal', // Reset to normal. Use fontFamily only
  }
  styles.push(fontStyle)
  return styles
}

const AppText = (props: TextProps & { children?: any }) => {
  const overrideStyle = props.style || undefined
  const finalStyle: StyleProp<TextStyle> = [appTextStyles.default]
  if (Array.isArray(overrideStyle)) {
    finalStyle.push(...overrideStyle)
  } else if (overrideStyle) {
    finalStyle.push(overrideStyle)
  }
  fontWeightTransformAndroid(finalStyle as TextStyle[])
  return (
    <Text allowFontScaling={false} {...props} style={finalStyle}>
      {props.children}
    </Text>
  )
}

export default AppText

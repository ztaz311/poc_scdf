import { RouteProp } from '@react-navigation/native'
import { observer } from 'mobx-react'
import React, { useCallback, useEffect, useState } from 'react'
import { Image, NativeModules, Platform, StyleSheet, View } from 'react-native'
import { Images } from '../../constants/appImages'
import { bottomTabsHeight } from '../../constants/constants'
import { useTheme } from '../../contexts/ThemeContext'
import HomePanel from '../../screens/landing/home/home-panel.screen'
import InboxMessagesService from '../../services/inbox-messages.service'
import LandingDataService from '../../services/landing-data.service'
import AppText from '../AppText'
import { createCustomTabNavigator } from './TabNavigator'

export enum BottomTabsEnum {
  HOME = 'Home',
  SAVED = 'Saved',
  Trends = 'Trends',
  INBOX = 'Inbox',
  MORE = 'More',
}

type Props = {}

const Tab = createCustomTabNavigator()
const communicateWithNative = NativeModules.CommunicateWithNative
export const getTabIcon = (routeName: BottomTabsEnum) => {
  switch (routeName) {
    case BottomTabsEnum.HOME:
      return Images.bottomTabHomeIcon
    case BottomTabsEnum.INBOX:
      return Images.bell
    case BottomTabsEnum.Trends:
      return Images.bottomTabTrendsIcon
    case BottomTabsEnum.SAVED:
      return Images.favIcon
    case BottomTabsEnum.MORE:
      return Images.bottomTabMoreIcon
    default:
      return -1
  }
}

const BottomTabs: React.FC<Props> = () => {
  const { themeColors } = useTheme()
  const [fetchedNotifications, setFetchedNotifications] = useState(false)

  useEffect(() => {
    let isSubscribed = true
    // Show red dot and send seen list to native
    // InboxMessagesService.fetchMessageList().then(() => {
    //   if (isSubscribed) {
    //     setFetchedNotifications(true)
    //   }
    //   communicateWithNative.sendSeenMessageList(JSON.stringify(InboxMessagesService.seenMessages))
    // })
    return () => {
      isSubscribed = false
    }
  }, [])
  const renderTabBarIcon = useCallback(
    (route: RouteProp<Record<string, object | undefined>, string>) => {
      return ({ focused }: { focused: boolean }) => {
        const haveNewInbox =
          route.name === BottomTabsEnum.INBOX &&
          (InboxMessagesService.hasUnreadNotification ||
            (fetchedNotifications && InboxMessagesService.messageList.filter(item => !item.seen).length > 0))
        const isShowRedDot = haveNewInbox

        return (
          <View style={{ marginTop: -3 }}>
            <Image
              style={[
                styles.bottomTabIcon,
                { tintColor: focused ? themeColors.primaryColor : themeColors.inactiveColor },
              ]}
              source={getTabIcon(route.name as BottomTabsEnum)}
            />
            {isShowRedDot && <View style={styles.redDot} />}
          </View>
        )
      }
    },
    [
      fetchedNotifications,
      themeColors.primaryColor,
      themeColors.inactiveColor,
      InboxMessagesService.hasUnreadNotification,
      InboxMessagesService.messageList,
    ],
  )

  const getTabLabel = (routeName: BottomTabsEnum) => {
    switch (routeName) {
      case BottomTabsEnum.HOME:
        return 'Home'
      case BottomTabsEnum.INBOX:
        return 'Inbox'
      case BottomTabsEnum.Trends:
        return 'Trends'
      case BottomTabsEnum.SAVED:
        return 'SAVED'
      case BottomTabsEnum.MORE:
        return 'MORE'
      default:
        return ''
    }
  }

  const renderTabBarLabel = (route: RouteProp<Record<string, object | undefined>, string>) => {
    return ({ focused }: { focused: boolean }) => {
      return (
        <AppText
          style={[
            styles.bottomTabLabel,
            {
              color: focused ? themeColors.primaryColor : themeColors.inactiveColor,
            },
          ]}>
          {getTabLabel(route.name as BottomTabsEnum)}
        </AppText>
      )
    }
  }

  return (
    <Tab.Navigator
      tabBarStyle={[
        styles.tabBarStyle,
        {
          display: LandingDataService.isShowBottomBar ? 'flex' : 'none',
          backgroundColor: themeColors.primaryBackground,
          borderTopColor: themeColors.primaryBackground,
          borderTopWidth: 1,
          height: bottomTabsHeight,
          paddingTop: Platform.OS === 'android' ? undefined : 16,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.32,
          shadowRadius: 5.46,

          elevation: 9,
        },
      ]}
      screenOptions={({ route }) => ({
        tabBarIcon: renderTabBarIcon(route),
        tabBarLabel: renderTabBarLabel(route),
      })}>
      <Tab.Screen
        key={BottomTabsEnum.HOME}
        name={BottomTabsEnum.HOME}
        component={HomePanel}
        listeners={{
          tabPress: e => {
            console.log('e', e)
          },
        }}
      />
    </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  tabBarStyle: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  bottomTabIcon: {
    width: 25,
    height: 25,
    resizeMode: 'center',
  },
  bottomTabLabel: {
    fontSize: 12,
    fontWeight: '400',
  },
  redDot: {
    width: 14,
    height: 14,
    right: -4,
    top: -4,
    borderRadius: 100 / 2,
    backgroundColor: '#E82370',
    position: 'absolute',
  },
})

export default observer(BottomTabs)

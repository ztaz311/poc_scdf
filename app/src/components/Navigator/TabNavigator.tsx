import { BottomTabNavigationOptions } from '@react-navigation/bottom-tabs'
import {
  createNavigatorFactory,
  DefaultNavigatorOptions,
  NavigationHelpersContext,
  ParamListBase,
  RouteProp,
  TabActionHelpers,
  TabActions,
  TabNavigationState,
  TabRouter,
  TabRouterOptions,
  useNavigationBuilder,
} from '@react-navigation/native'
import React from 'react'
import {
  ColorValue,
  ImageStyle,
  NativeModules,
  Pressable,
  StyleProp,
  StyleSheet,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Colors } from '../../constants/appColors'
import { useTheme } from '../../contexts/ThemeContext'
import NavigationDataService from '../../services/navigation-data.service'
import { BottomTabsEnum } from './BottomTabs'

const communicateWithNative = NativeModules.CommunicateWithNative

// Props accepted by the view
type TabNavigationConfig = {
  tabBarStyle?: StyleProp<ViewStyle>
  labelStyle?: StyleProp<TextStyle>
  iconStyle?: StyleProp<ImageStyle>
  activeColor: ColorValue
  inactiveColor: ColorValue
}

type TabNavigationEventMap = {
  tabPress: {
    data: { isAlreadyFocused: boolean }
    canPreventDefault: true
  }
}

// The props accepted by the component is a combination of 3 things
type Props = DefaultNavigatorOptions<BottomTabNavigationOptions> & TabRouterOptions & TabNavigationConfig

// const FULL_SCREEN_LIST = [
//   reactScreens.FavouriteList,
//   reactScreens.RNSearchLocation,
//   reactScreens.ETAFavouriteList,
//   reactScreens.ETAScreen,
//   reactScreens.Inbox,
//   reactScreens.FavouriteEditInfo,
//   reactScreens.FavouriteAddNew,
//   reactScreens.Voucher,
// ]

function TabNavigator(props: Props) {
  const { state, navigation, descriptors } = useNavigationBuilder<
    TabNavigationState<ParamListBase>,
    TabRouterOptions,
    TabActionHelpers<ParamListBase>,
    BottomTabNavigationOptions,
    TabNavigationEventMap
  >(TabRouter, {
    screenOptions: props.screenOptions,
    children: props.children,
    initialRouteName: props.initialRouteName,
  })
  const { themeColors } = useTheme()
  const currentRoute = state.routeNames[state.index]
  const existChild = state.routes.some(item => item?.state?.index && item.state.index > 0)
  const isShowBottomTab = !existChild && (state.index < Object.keys(BottomTabsEnum).length || currentRoute === 'Notify')
  // whitelisting bottomTab display for 'Notify' screen. Todo: refactor;
  // change BottomTab color for only Home and More tab
  const isFullScreen = state.index === 1 || state.index === 2

  // useEffect(() => {
  //   if (Platform.OS === 'ios') {
  //     communicateWithNative.changeRNScreen(FULL_SCREEN_LIST.includes(state.routeNames[state.index]) ? true : false)
  //   }
  // }, [state.index, state.routeNames])

  // const handleInboxNavigationEvents = useCallback(
  //   (data: any) => {
  //     const isOpenInboxList = data.NOTIFICATION_ID < 0
  //     InboxMessagesService.updateSeenList(data.NOTIFICATION_ID)
  //     if (!isOpenInboxList) {
  //       InboxMessagesService.fetchMessageList().then(() => {
  //         const notificationData = InboxMessagesService.messageList.find(
  //           item => item.notificationId === data.NOTIFICATION_ID,
  //         )
  //         if (notificationData) {
  //           navigation.navigate(reactScreens.Inbox, {
  //             notificationData,
  //           })
  //         }
  //       })
  //     } else {
  //       navigation.navigate(reactScreens.Inbox)
  //     }
  //   },
  //   [navigation],
  // )

  // useBackHandler(
  //   useCallback(() => {
  //     if ([0, 1, 2, 3].includes(state.index)) {
  //       NavigationDataService.finishActivity()
  //     }
  //     return false
  //   }, [state.index]),
  // )

  const handleSeparateTabLogic = (route: RouteProp<Record<string, object | undefined>, string>) => {
    // Reset everything when clicking home
    if (route.name === BottomTabsEnum.HOME) {
      NavigationDataService.handleResetHome()
    }
  }

  return (
    <NavigationHelpersContext.Provider value={navigation}>
      <View style={[{ flex: 1, borderColor: 'transparent' }]} pointerEvents="box-none">
        {state.routes.map((route, i) => {
          if (route.key !== state.routes[state.index].key) {
            return null
          }

          return (
            <View
              key={route.key}
              pointerEvents="box-none"
              style={[
                StyleSheet.absoluteFill,
                {
                  display: i === state.index ? 'flex' : 'none',
                },
              ]}>
              {descriptors[route.key].render()}
            </View>
          )
        })}
      </View>
      {isShowBottomTab && (
        <SafeAreaView
          style={[
            {
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              paddingVertical: 8,
              backgroundColor: isFullScreen ? Colors.primaryBackground : themeColors.primaryBackground,
            },
            props.tabBarStyle,
          ]}>
          {state.routes.slice(0, Object.keys(BottomTabsEnum).length).map(route => (
            <Pressable
              key={route.key}
              onPress={() => {
                const event = navigation.emit({
                  type: 'tabPress',
                  target: route.key,
                  canPreventDefault: true,
                  data: {
                    isAlreadyFocused: route.key === state.routes[state.index].key,
                  },
                })
                if (!event.defaultPrevented) {
                  navigation.dispatch({
                    ...TabActions.jumpTo(route.name),
                    target: state.key,
                  })
                }
                handleSeparateTabLogic(route)
              }}
              style={{ flex: 1, alignItems: 'center' }}>
              <>
                {descriptors[route.key].options.tabBarIcon?.({
                  focused: route.key === state.routes[state.index].key,
                  color: '',
                  size: 45,
                })}
                {descriptors[route.key].options.tabBarLabel?.({
                  focused: route.key === state.routes[state.index].key,
                  color: '',
                  position: 'below-icon',
                })}
              </>
            </Pressable>
          ))}
        </SafeAreaView>
      )}
    </NavigationHelpersContext.Provider>
  )
}

export const createCustomTabNavigator = createNavigatorFactory<
  TabNavigationState<ParamListBase>,
  BottomTabNavigationOptions,
  TabNavigationEventMap,
  typeof TabNavigator
>(TabNavigator)

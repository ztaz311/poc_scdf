import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'
import { reactScreens } from '../../constants/screensName'
import DriveTimeScreen from '../../screens/drive-trend/chart-screens/drive-time.screen'
import DriveTrendEnergyScreen from '../../screens/drive-trend/chart-screens/energy.screen'
import TotalERPFeeScreen from '../../screens/drive-trend/chart-screens/total-erp-fee.screen'
import TotalParkingFeeScreen from '../../screens/drive-trend/chart-screens/total-parking-fee.screen'
import DriveTrendScreen from '../../screens/drive-trend/drive-trend.screen'
import VehicleDetailsScreen from '../../screens/obu-settings/vehicle-details.screens'
import VehicleSettingsScreen from '../../screens/obu-settings/vehicle-settings.screens'
import { defaultHeader } from './constants'
const Stack = createStackNavigator()

const DriveTrendStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...defaultHeader,
      }}>
      <Stack.Screen name={reactScreens.DriveTrend} key={reactScreens.DriveTrend} component={DriveTrendScreen} />
      <Stack.Screen name={reactScreens.DriveTime} key={reactScreens.DriveTime} component={DriveTimeScreen} />
      <Stack.Screen
        name={reactScreens.TotalParkingFee}
        key={reactScreens.TotalParkingFee}
        component={TotalParkingFeeScreen}
      />
      <Stack.Screen name={reactScreens.TotalERPFee} key={reactScreens.TotalERPFee} component={TotalERPFeeScreen} />
      <Stack.Screen
        name={reactScreens.DriveTrendEnergy}
        key={reactScreens.DriveTrendEnergy}
        component={DriveTrendEnergyScreen}
      />
      <Stack.Screen
        name={reactScreens.VehicleSettings}
        key={reactScreens.VehicleSettings}
        component={VehicleSettingsScreen}
      />
      <Stack.Screen
        name={reactScreens.VehicleDetails}
        key={reactScreens.VehicleDetails}
        component={VehicleDetailsScreen}
      />
    </Stack.Navigator>
  )
}
export default DriveTrendStack

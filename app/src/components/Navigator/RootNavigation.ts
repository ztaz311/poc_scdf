import * as React from 'react'
export const navigationRef: any = React.createRef()
export const isNavigationReady: any = React.createRef()

export function navigate(name: string, params?: any) {
  if (isNavigationReady.current && navigationRef.current) {
    navigationRef.current?.navigate(name, params)
  } else {
    console.log('not initialized')
  }
}
export function goBack() {
  if (isNavigationReady.current && navigationRef.current) {
    setTimeout(() => {
      if (navigationRef.current?.canGoBack()) {
        navigationRef.current?.goBack()
      }
    }, 0)
  } else {
    console.log('not initialized')
  }
}

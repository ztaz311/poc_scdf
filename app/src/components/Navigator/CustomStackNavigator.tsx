import { BottomTabNavigationOptions } from '@react-navigation/bottom-tabs'
import {
  createNavigatorFactory,
  DefaultNavigatorOptions,
  NavigationHelpersContext,
  ParamListBase,
  StackActionHelpers,
  StackNavigationState,
  StackRouter,
  StackRouterOptions,
  useNavigationBuilder,
} from '@react-navigation/native'
import { StackNavigationEventMap, StackNavigationOptions } from '@react-navigation/stack/lib/typescript/src/types'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { reactScreens } from '../../constants/screensName'

// The props accepted by the component is a combination of 3 things
type Props = DefaultNavigatorOptions<StackNavigationOptions> & StackRouterOptions

const FULL_SCREEN_LIST = [
  reactScreens.FavouriteList,
  reactScreens.RNSearchLocation,
  reactScreens.ETAFavouriteList,
  reactScreens.ETAScreen,
  reactScreens.Inbox,
  reactScreens.FavouriteEditInfo,
  reactScreens.FavouriteAddNew,
  reactScreens.Voucher,
]

function CustomStackNavigator(props: Props) {
  const { state, navigation, descriptors } = useNavigationBuilder<
    StackNavigationState<ParamListBase>,
    StackRouterOptions,
    StackActionHelpers<ParamListBase>,
    StackNavigationOptions,
    StackNavigationEventMap
  >(StackRouter, {
    screenOptions: props.screenOptions,
    children: props.children,
    initialRouteName: props.initialRouteName,
  })

  return (
    <NavigationHelpersContext.Provider value={navigation}>
      <View
        style={{
          flex: 1,
          borderColor: 'transparent',
          // TODO: _IMPORTANT: above border was added to fix the bottomSheet
          // clipping issue, but the root cause for the issue as well as why
          // this solution works is not clear at the moment this fix was done
        }}
        pointerEvents="box-none">
        {state.routes.map((route, i) => {
          if (route.key !== state.routes[state.index].key) {
            return null
          }

          return (
            <View
              key={route.key}
              pointerEvents="box-none"
              style={[
                StyleSheet.absoluteFill,
                {
                  display: i === state.index ? 'flex' : 'none',
                },
              ]}>
              {descriptors[route.key].render()}
            </View>
          )
        })}
      </View>
    </NavigationHelpersContext.Provider>
  )
}

export const createCustomStackNavigator = createNavigatorFactory<
  StackNavigationState<ParamListBase>,
  BottomTabNavigationOptions,
  StackNavigationEventMap,
  typeof CustomStackNavigator
>(CustomStackNavigator)

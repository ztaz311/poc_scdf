import * as React from 'react'
export const navigationHybridRef: any = React.createRef()
export const isNavigationReadyHybrid: any = React.createRef()

export function navigate(name: string, params?: any) {
  if (isNavigationReadyHybrid.current && navigationHybridRef.current) {
    navigationHybridRef.current?.navigate(name, params)
  } else {
    console.log('not initialized')
  }
}
export function goBack() {
  if (isNavigationReadyHybrid.current && navigationHybridRef.current) {
    setTimeout(() => {
      if (navigationHybridRef.current?.canGoBack()) {
        navigationHybridRef.current?.goBack()
      }
    }, 0)
  } else {
    console.log('not initialized')
  }
}

import { TransitionPresets } from '@react-navigation/stack'

export const defaultHeader = {
  headerShown: false,
  ...TransitionPresets.SlideFromRightIOS,
}

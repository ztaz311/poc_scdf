import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'
import { reactScreens } from '../../constants/screensName'
import ListPairedVehicles from '../../screens/landing/home/obu/ListPairedVehicles.screen'
import ObuSettingsScreen from '../../screens/obu-settings/obu-settings.screen'
import RoadMessagesScreen from '../../screens/obu-settings/road-messages.screen'
import VehicleDetailsScreen from '../../screens/obu-settings/vehicle-details.screens'
import { default as VehicleSettingsScreen } from '../../screens/obu-settings/vehicle-settings.screens'
import { defaultHeader } from './constants'
const Stack = createStackNavigator()

const OBUSettingsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...defaultHeader,
      }}>
      <Stack.Screen name={reactScreens.OBUSettings} key={reactScreens.OBUSettings} component={ObuSettingsScreen} />
      <Stack.Screen name={reactScreens.RoadMessages} key={reactScreens.RoadMessages} component={RoadMessagesScreen} />
      <Stack.Screen
        name={reactScreens.VehicleSettings}
        key={reactScreens.VehicleSettings}
        component={VehicleSettingsScreen}
      />

      <Stack.Screen
        name={reactScreens.VehicleDetails}
        key={reactScreens.VehicleDetails}
        component={VehicleDetailsScreen}
      />
      <Stack.Screen
        name={reactScreens.ListPairedVehicles}
        key={reactScreens.ListPairedVehicles}
        component={ListPairedVehicles}
      />
    </Stack.Navigator>
  )
}
export default OBUSettingsStack

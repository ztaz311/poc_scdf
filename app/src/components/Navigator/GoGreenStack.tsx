import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'
import { reactScreens } from '../../constants/screensName'
import GoGreenScreen from '../../screens/drive-trend/go-green.screen'
import VehicleDetailsScreen from '../../screens/obu-settings/vehicle-details.screens'
import VehicleSettingsScreen from '../../screens/obu-settings/vehicle-settings.screens'
import { defaultHeader } from './constants'
const Stack = createStackNavigator()

const GoGreenStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...defaultHeader,
      }}>
      <Stack.Screen name={reactScreens.GoGreen} key={reactScreens.GoGreen} component={GoGreenScreen} />
      <Stack.Screen
        name={reactScreens.VehicleSettings}
        key={reactScreens.VehicleSettings}
        component={VehicleSettingsScreen}
      />
      <Stack.Screen
        name={reactScreens.VehicleDetails}
        key={reactScreens.VehicleDetails}
        component={VehicleDetailsScreen}
      />
    </Stack.Navigator>
  )
}
export default GoGreenStack

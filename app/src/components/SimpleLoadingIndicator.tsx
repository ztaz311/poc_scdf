import React, { memo } from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'

type Props = {}

const SimpleLoadingIndicatorComponent: React.FC<Props> = () => {
  return (
    <View style={[StyleSheet.absoluteFill, styles.loadingWrapper]}>
      <ActivityIndicator size="large" color={'#782EB1'} />
    </View>
  )
}

const styles = StyleSheet.create({
  loadingWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
})

export default memo(SimpleLoadingIndicatorComponent)

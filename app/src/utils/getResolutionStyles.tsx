import { Platform } from 'react-native'
import { deviceWidth } from '../constants/constants'

export const WIDTH_BREAKPOINT = 393

const ExploreBottomSheet = {
  snapPoints: [Platform.OS === 'ios' ? 165 : 145, '55%', '95%'],
  snapPointsSearch: [Platform.OS === 'ios' ? 145 : 125, '55%', '95%'],
  snapPointsCarPark: [Platform.OS === 'ios' ? 165 : 135, '55%', '95%'],
  snapPointsCarParkSearch: [Platform.OS === 'ios' ? 225 : 195, '55%', '95%'],
  poiDetailSnapPoints: [Platform.OS === 'ios' ? 90 : 80, '55%', '95%'],
}

export const largeScreenValuesAndroid = {
  AmenityList: {
    fontSize10: 11,
  },
  ExploreBottomSheet,
  BottomSheet: {
    snapPoints: [145, '55%', '95%'],
    snapPointsSearch: [125, '55%', '95%'],
    snapPointsCarPark: [190, '55%', '95%'],
    snapPointsCarParkSearch: [170, '55%', '95%'],
    snapPointChoose: ['50%', '95%'],
  },
  SearchArea: {
    fontSize12: 12,
  },
  CameraImages: {
    fontSize16: 16,
  },
  ERPSearch: {
    fontSize16: 16,
  },
  ParkingCalculator: {
    fontSize18: 16,
    timePickerInput: {
      paddingVertical: 10,
    },
  },
  AmenitiesView: {
    fontSize16: 16,
    fontSize14: 14,
  },
}
export const largeScreenValuesIOS = {
  AmenityList: {
    fontSize10: 14,
  },
  ExploreBottomSheet,
  BottomSheet: {
    snapPoints: [145, '55%', '95%'],
    snapPointsSearch: [125, '55%', '95%'],
    snapPointsCarPark: [195, '55%', '95%'],
    snapPointsCarParkSearch: [170, '55%', '95%'],
    snapPointChoose: ['50%', '95%'],
  },
  SearchArea: {
    fontSize12: 15,
  },
  CameraImages: {
    fontSize16: 16,
  },
  ERPSearch: {
    fontSize16: 16,
  },
  ParkingCalculator: {
    fontSize18: 16,
    timePickerInput: {
      paddingVertical: 10,
    },
  },
  AmenitiesView: {
    fontSize16: 16,
    fontSize14: 14,
  },
}

export const smallScreenValuesAndroid = {
  AmenityList: {
    fontSize10: 10,
  },
  ExploreBottomSheet,
  BottomSheet: {
    snapPoints: [145, '60%', '95%'],
    snapPointsSearch: [125, '60%', '95%'],
    snapPointsCarPark: [190, '55%', '95%'],
    snapPointsCarParkSearch: [195, '55%', '95%'],
    snapPointChoose: ['50%', '95%'],
  },
  SearchArea: {
    fontSize12: 11,
  },
  CameraImages: {
    fontSize16: 15,
  },
  ERPSearch: {
    fontSize16: 15,
  },
  ParkingCalculator: {
    fontSize18: 15,
    timePickerInput: {
      paddingVertical: 6,
    },
  },
  AmenitiesView: {
    fontSize16: 15,
    fontSize14: 13,
  },
}

export const smallScreenValuesIOS = {
  AmenityList: {
    fontSize10: 13,
  },
  ExploreBottomSheet,
  BottomSheet: {
    snapPoints: [145, '60%', '95%'],
    snapPointsSearch: [125, '60%', '95%'],
    snapPointsCarPark: [185, '55%', '95%'],
    snapPointsCarParkSearch: [170, '55%', '95%'],
    snapPointChoose: ['50%', '95%'],
  },
  SearchArea: {
    fontSize12: 12,
  },
  CameraImages: {
    fontSize16: 16,
  },
  ERPSearch: {
    fontSize16: 16,
  },
  ParkingCalculator: {
    fontSize18: 16,
    timePickerInput: {
      paddingVertical: 10,
    },
  },
  AmenitiesView: {
    fontSize16: 16,
    fontSize14: 14,
  },
}

export const getResolutionStyles = () => {
  if (Platform.OS === 'android') {
    return deviceWidth > WIDTH_BREAKPOINT ? largeScreenValuesAndroid : smallScreenValuesAndroid
  } else {
    return deviceWidth > WIDTH_BREAKPOINT ? largeScreenValuesIOS : smallScreenValuesIOS
  }
}

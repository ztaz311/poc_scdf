import AsyncStorage from '@react-native-async-storage/async-storage'

export const appStorageController = {
  getItem: async (key: string) => {
    try {
      return await AsyncStorage.getItem(key)
    } catch (e) {
      console.log('getItem error', e)
      return null
    }
  },
  setItem: async (key: string, value: any) => {
    try {
      return await AsyncStorage.setItem(key, value)
    } catch (e) {
      console.log('setItem error', e)
    }
  },
  removeItem: async (key: string) => {
    try {
      return await AsyncStorage.removeItem(key)
    } catch (e) {
      console.log('removeItem error', e)
    }
  },
}

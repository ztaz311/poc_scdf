import { format } from 'date-fns'
import { SearchLocationModel } from '../models/search-location.model'

export const currencyFormat = (value: number) => `$${value.toFixed(2)}`

export const isOperatingErp = (startTime: string, endTime: string, compareTime?: Date) => {
  const regex = new RegExp(':', 'g')
  const currentTime = format(compareTime || new Date(), 'HH:mm')

  return (
    parseInt(startTime.replace(regex, ''), 10) <= parseInt(currentTime.replace(regex, ''), 10) &&
    parseInt(currentTime.replace(regex, ''), 10) < parseInt(endTime.replace(regex, ''), 10)
  )
}

export const isSameLocation = (location1?: SearchLocationModel, location2?: SearchLocationModel) =>
  location1 && location2 && location1.lat === location2.lat && location1.long === location2.long

export const deepClone = (jsonObj: { [key: string]: any } | { [key: string]: any }[]) =>
  JSON.parse(JSON.stringify(jsonObj))

export const convertTime24To12 = (time: string) => {
  const [hour, minute] = time.split(':')
  const hourInt = parseInt(hour, 10)
  const hour12 = hourInt > 12 ? hourInt - 12 : hourInt
  const ampm = hourInt >= 12 ? 'pm' : 'am'
  return `${`${hour12}`.padStart(2, '0')}:${minute}${ampm}`
}
export const maskNumber = (string: string) => {
  const numberString = String(string)
  const masked = '*'.repeat(numberString.length - 4) + numberString.slice(-4)
  return masked
}

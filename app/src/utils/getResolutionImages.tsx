import { Platform } from 'react-native'
import { deviceHeight, deviceWidth } from '../constants/constants'

export const iosImages = {
  proMax: {
    tutorialStep1: require('../../assets/images/tutor-promax-1.png'),
    tutorialStep2: require('../../assets/images/tutor-promax-2.png'),
    tutorialStep3: require('../../assets/images/tutor-promax-3.png'),
    tutorialStep4: require('../../assets/images/tutor-promax-4.png'),
  },
  SE: {
    tutorialStep1: require('../../assets/images/tutor-se-1.png'),
    tutorialStep2: require('../../assets/images/tutor-se-2.png'),
    tutorialStep3: require('../../assets/images/tutor-se-3.png'),
    tutorialStep4: require('../../assets/images/tutor-se-4.png'),
  },
}

export const androidImages = {
  large: {
    tutorialStep1: require('../../assets/images/tutor-large-1.png'),
    tutorialStep2: require('../../assets/images/tutor-large-2.png'),
    tutorialStep3: require('../../assets/images/tutor-large-3.png'),
    tutorialStep4: require('../../assets/images/tutor-large-4.png'),
  },
  small: {
    tutorialStep1: require('../../assets/images/tutor-small-1.png'),
    tutorialStep2: require('../../assets/images/tutor-small-2.png'),
    tutorialStep3: require('../../assets/images/tutor-small-3.png'),
    tutorialStep4: require('../../assets/images/tutor-small-4.png'),
  },
}

export const getResolutionImages = () => {
  if (Platform.OS === 'ios') {
    if (deviceWidth >= 375 && deviceHeight >= 812) {
      return iosImages.proMax
    }
    return iosImages.SE
  } else {
    if (deviceWidth >= 360 && deviceHeight >= 732) {
      return androidImages.large
    }
    return androidImages.small
  }
}

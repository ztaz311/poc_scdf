import { CarparkList } from '../models/carpark-calculator.model'

export const compareCarParkByPrice = (a: CarparkList, b: CarparkList): number => {
  if (a.currentHrRate.oneHrRate === b.currentHrRate.oneHrRate) {
    if (a.distance === b.distance) {
      return a.name.localeCompare(b.name, 'en')
    }
    return a.distance - b.distance
  }

  return a.currentHrRate.oneHrRate - b.currentHrRate.oneHrRate
}

export const compareCarParkByDistance = (a: CarparkList, b: CarparkList): number => {
  if (a.distance === b.distance) {
    if (a.currentHrRate.oneHrRate === b.currentHrRate.oneHrRate) {
      return a.name.localeCompare(b.name, 'en')
    }
    return a.currentHrRate.oneHrRate - b.currentHrRate.oneHrRate
  }
  return a.distance - b.distance
}

export const compareCarParkByAvailableLot = (a: CarparkList, b: CarparkList): number => {
  if (a.availablelot === b.availablelot) {
    if (a.distance === b.distance) {
      if (a.currentHrRate.oneHrRate === b.currentHrRate.oneHrRate) {
        return a.name.localeCompare(b.name, 'en')
      }
      return a.currentHrRate.oneHrRate - b.currentHrRate.oneHrRate
    }
    return a.distance - b.distance
  }
  return b.availablelot - a.availablelot
}

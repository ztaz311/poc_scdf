import { convertTime24To12 } from '../functions'

test('Convert 00 ', () => {
  expect(convertTime24To12('00:00')).toBe('00:00am')
})

test('Convert 6am', () => {
  expect(convertTime24To12('06:00')).toBe('06:00am')
})

test('Convert 12pm', () => {
  expect(convertTime24To12('12:00')).toBe('12:00pm')
})

test('Convert 12:30pm', () => {
  expect(convertTime24To12('12:30')).toBe('12:30pm')
})

test('Convert 13:30', () => {
  expect(convertTime24To12('13:01')).toBe('01:01pm')
})

test('Convert 23:00', () => {
  expect(convertTime24To12('23:00')).toBe('11:00pm')
})

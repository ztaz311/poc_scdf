import AlertService from '../services/alert.service'

export const showAlert = (
  title: string,
  message: string,
  buttonText: string,
  onSubmit: () => void,
  submitStyle?: 'default' | 'cancel' | 'destructive' | undefined,
) => {
  AlertService.alert(title, message, [
    { text: 'Cancel' },
    {
      text: buttonText,
      onPress: () => {
        onSubmit()
      },
      style: submitStyle,
    },
  ])
}

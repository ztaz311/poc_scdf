export const diffMinutes = (date1: Date, date2: Date) => {
  date1.setSeconds(0)
  date2.setSeconds(0)
  const d2 = date2.getTime()
  const d1 = new Date(date1).getTime()
  return Math.round((d2 - d1) / 60000)
}

export const convertMinutesToHoursMinutes = (minutes: string | number) => {
  if (typeof minutes === 'string') {
    minutes = parseInt(minutes, 10)
  }

  if (minutes >= 60) {
    const hours = Math.floor(minutes / 60)
    const mins = minutes % 60
    return `${hours} hour${hours > 1 ? 's' : ''} ${mins} min${mins > 1 ? 's' : ''}`
  } else {
    return `${minutes} min${minutes > 1 ? 's' : ''}`
  }
}

export const convertMinutesToHM = (minutes: string | number) => {
  if (typeof minutes === 'string') {
    minutes = parseInt(minutes, 10)
  }

  if (minutes >= 60) {
    const hours = Math.floor(minutes / 60)
    const mins = minutes % 60
    return `${hours}h ${mins}m`
  } else {
    return `${minutes} min${minutes > 1 ? 's' : ''}`
  }
}

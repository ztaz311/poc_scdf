import { NativeModules } from 'react-native'

class Logger {
  private logNative(tag: string, message: string, ...optionalParams: any[]) {
    NativeModules.CommunicateWithNative?.logNative?.(
      tag,
      optionalParams.reduce<string>((pre, current) => pre.concat(' ').concat(String(current)), message),
    )
  }

  logNativeD(message: string, ...optionalParams: any[]) {
    this.logNative('debug', message, optionalParams)
  }

  logNativeI(message: string, ...optionalParams: any[]) {
    this.logNative('info', message, optionalParams)
  }

  logNativeW(message: string, ...optionalParams: any[]) {
    this.logNative('warning', message, optionalParams)
  }

  logNativeE(message: string, ...optionalParams: any[]) {
    this.logNative('error', message, optionalParams)
  }

  logConsole(message?: any, ...optionalParams: any[]) {
    console.log(message, optionalParams)
  }
}

export const logger = new Logger()

import { DeviceEventEmitter, NativeEventEmitter, NativeModules, Platform } from 'react-native'
import { handleNativeRequest } from './share-business-logic.mapper'
const { ShareBusinessLogic: ShareBusinessLogicNative } = NativeModules

interface AnyObject {
  [key: string]: any
}

export interface EventData {
  requestId: string
  functionName: string
  data: AnyObject
}

class ShareBusinessLogicImpl {
  readonly constants = {
    SEND_THEME_CHANGE_EVENT: ShareBusinessLogicNative.SEND_THEME_CHANGE_EVENT,
  }

  async processRequest(
    requestId: string,
    handleFunction: () => Promise<AnyObject | null | undefined> | AnyObject | null | undefined,
  ) {
    try {
      let result = handleFunction()
      if (result instanceof Promise) {
        result = await result
      }

      this.finishedRequest(requestId, result)
    } catch (e: unknown) {
      let message = 'Unknown'
      if (typeof e === 'string') {
        message = e.toUpperCase()
      } else if (e instanceof Error && e.message) {
        message = e.message
      }
      this.failedRequest(requestId, message)
    }
  }

  private finishedRequest(requestId: string, data: AnyObject | null = {}) {
    ShareBusinessLogicNative.finishedRequest(requestId, {
      data,
    })
  }

  private failedRequest(requestId: string, errorMessage: string) {
    ShareBusinessLogicNative.failedRequest(requestId, errorMessage)
  }

  /**
   * Handle request from native. This is used to share business logic between native & React native
   */
  startHandleShareBusinessLogic(): void {
    const emitter =
      Platform.OS === 'ios' ? new NativeEventEmitter(NativeModules.ReactNativeEventEmitter) : DeviceEventEmitter
    emitter.addListener(
      ShareBusinessLogic.constants.SHARE_BUSINESS_LOGIC_REQUEST,
      (event: EventData) => {
        // This is called whenever we receive a `ShareBusinessLogic.constants.REQUEST_EVENT_NAME` event from native code.
        handleNativeRequest(event)
      },
      this,
    )
  }
}

const ShareBusinessLogic = new ShareBusinessLogicImpl()

export default ShareBusinessLogic

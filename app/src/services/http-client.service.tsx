import axios, { AxiosError, AxiosRequestConfig, Canceler } from 'axios'
import { NativeModules, Platform } from 'react-native'
import Toast from 'react-native-toast-message'
import { logger } from '../utils/logger'
import AuthService from './auth.service'
import { ShareCollectionErrorCode, canHandleErrorAtSource } from './http-error-handlers.service'
import NavigationDataService from './navigation-data.service'
import { CarDetailsResponse } from './user.service'

export const HTTPNetworkError = {
  parametersNil: 'Error Found : Parameters are nil.',
  headersNil: 'Error Found : Headers are Nil',
  encodingFailed: 'Error Found : Parameter Encoding failed.',
  decodingFailed: 'Error Found : Unable to Decode the data.',
  missingURL: 'Error Found : The URL is nil.',
  couldNotParse: 'Error Found : Unable to parse the JSON response.',
  noData: 'Error Found : The data from API is Nil.',
  FragmentResponse: "Error Found : The API's response's body has fragments.",
  UnwrappingError: 'Error Found : Unable to unwrape the data.',
  dataTaskFailed: 'Error Found : The Data Task object failed.',
  success: 'Successful Network Request',
  authenticationError: 'Error Found : You must be Authenticated',
  badRequest: 'Error Found : Bad Request',
  pageNotFound: 'Error Found : Page/Route rquested not found.',
  failed: 'Error Found : Network Request failed',
  serverSideError: 'Error Found : Server error',
}

export const baseURL = NativeModules.RNAuth?.getConstants()?.API_BASE_URL?.replace(/\\/g, '')
logger.logConsole('http-client.service baseURL: ', baseURL)

const sendLogToFirebase = (name: string, data: any) => {
  NavigationDataService.sendAnalyticsEventsToNative('system_event', 'api_error', '[app]', false, {
    message: `${data?.response?.code || data?.code || ''} - [${data?.config?.url || ''}] - ${data?.message || ''}`,
  })
}

const HttpService = axios.create({
  baseURL: baseURL,
  headers: {
    'x-breeze-platform': Platform.OS,
  },
})

HttpService.interceptors.request.use(
  async function (config) {
    logger.logConsole('http-client.service onRequestFulfilled = ', config.method, config.url)
    await injectIdToken(config)
    return config
  },
  function (error) {
    logger.logConsole('http-client.service onRejected', error)
    sendLogToFirebase(`HTTPNetworkError from RN - Error Request`, error)
    return Promise.reject(error)
  },
)

HttpService.interceptors.response.use(
  function (response) {
    logger.logConsole('http-client.service onResponse = ', response.config.method, response.config.url)
    return response
  },
  function (error: AxiosError<CarDetailsResponse | ShareCollectionErrorCode>) {
    const statusCode: number | undefined = error.response?.status

    logger.logConsole('http-client.service onResponseError = ', error.code, error.message)
    if (canHandleErrorAtSource(error)) return Promise.reject(error)
    if (statusCode) {
      switch (true) {
        case statusCode === 401:
          Toast.show({
            text1: HTTPNetworkError.authenticationError,
            type: 'error',
          })
          break
        case statusCode >= 400 && statusCode <= 499:
          Toast.show({
            text1: HTTPNetworkError.badRequest,
            type: 'error',
          })
          break
        case statusCode >= 500 && statusCode <= 599:
          Toast.show({
            text1: HTTPNetworkError.serverSideError,
            type: 'error',
          })
          break
        default:
          // Toast.show({
          //   text1: HTTPNetworkError.failed,
          //   type: 'error',
          // })
          break
      }

      const errorData = error.response?.data as any
      sendLogToFirebase(`HTTPNetworkError from RN - Error`, errorData)
    } else {
      sendLogToFirebase(`HTTPNetworkError from RN - Error Response`, error)
      if (error.message.includes('Network Error')) {
        // Toast.show({
        //   text1: HTTPNetworkError.failed,
        //   type: 'error',
        // })
      }
    }
    return Promise.reject(error)
  },
)

const injectIdToken = async (config: AxiosRequestConfig) => {
  const headers = config.headers || {}

  const idToken = await AuthService.getIdToken()

  headers.Authorization = `Bearer ${idToken}`

  config.baseURL = baseURL
}

export interface ICancelerCall {
  call?: Canceler
}

export default HttpService

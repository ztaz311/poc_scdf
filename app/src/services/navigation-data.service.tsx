import { RouteProp } from '@react-navigation/native'
import { action, makeAutoObservable } from 'mobx'
import { NativeModules, Platform } from 'react-native'
import { AnalyticsExtra } from '../models/analytics.model'
const communicateWithNative = NativeModules.CommunicateWithNative

export enum AndroidOpenAs {
  Fragment = 'Fragment',
  Activity = 'Activity',
}

export interface ParamsFromNative {
  openAs?: AndroidOpenAs // Android only
  fragmentId?: string
  idToken?: string
  [key: string]: any
  erpId?: number
  isSelected?: boolean
  erpSelTS: number
  isShowCarparkListButton?: boolean
  lat?: string // center location for parking calculator
  long?: string //center location for parking calculator
}

interface NavigationNativeData {
  rootTag?: number
  toScreen?: string
  paramsFromNative?: any
}

export type LocationDataEventType = {
  eventValue: string
  screenName: string
  locationName: string
  latitude: string
  longitude: string
}

class NavigationDataServiceImpl {
  navigationNativeData: NavigationNativeData[] = []
  constructor() {
    makeAutoObservable(
      this,
      {
        addNativeData: action,
        popNavigationNativeData: action,
      },
      {
        autoBind: true,
      },
    )
  }

  addNativeData(newData: NavigationNativeData) {
    this.navigationNativeData.push(newData)
  }

  popNavigationNativeData() {
    this.navigationNativeData.pop()
  }

  get currentNavigationNativeData() {
    return this.navigationNativeData[this.navigationNativeData.length - 1]
  }

  get rootTag() {
    return this.currentNavigationNativeData.rootTag
  }

  get toScreen() {
    return this.currentNavigationNativeData.toScreen
  }

  get paramsFromNative() {
    return this.currentNavigationNativeData.paramsFromNative
  }

  get backToNativeData() {
    const data = {
      openAs: this.paramsFromNative?.openAs || AndroidOpenAs.Activity,
      fragmentId: this.paramsFromNative?.fragmentId,
    }
    return data
  }

  /**
   * Open native screen with some params
   * @param screenName
   * @param navigationParams
   */
  openNativeScreen(screenName: string, navigationParams?: { [key: string]: any }) {
    if (Platform.OS === 'ios') {
      communicateWithNative.openNativeScreen(this.rootTag, screenName, navigationParams)
    } else {
      communicateWithNative.openNativeScreen(screenName, navigationParams)
    }
  }

  /**
   * Open native contact screen with some params
   */

  sendMessageToNative(screenName: string, navigationParams?: { [key: string]: any }) {
    console.log('send message to native', navigationParams, screenName)
    communicateWithNative.sendMessageToNative(navigationParams, screenName)
  }

  backToNative() {
    if (Platform.OS === 'ios') {
      const rootTag = this.rootTag
      communicateWithNative.backToNative(rootTag)
      return
    }

    // Android
    const data = this.backToNativeData
    if (!data.openAs) {
      console.warn('backToNative', 'openAs is missing')
    }
    if (data.openAs === AndroidOpenAs.Fragment && !data.fragmentId) {
      console.warn('backToNative', 'fragmentId is missing')
    }
    communicateWithNative.backToNative(data)
  }

  dismissNative() {
    if (Platform.OS === 'ios') {
      const rootTag = NavigationDataService.rootTag
      communicateWithNative.dismissNative(rootTag)
    } else {
      // communicateWithNative.backToNative();

      // Android
      const data = this.backToNativeData
      if (!data.openAs) {
        console.warn('backToNative', 'openAs is missing')
      }
      if (data.openAs === AndroidOpenAs.Fragment && !data.fragmentId) {
        console.warn('backToNative', 'fragmentId is missing')
      }
      communicateWithNative.backToNative(data)
    }
  }

  /**
   *
   * @param saveToLocal: to tell native side to save event so later we can use it to track user's action for other feature like calculating voucher.
   */
  sendAnalyticsEventsToNative(
    eventName: string,
    eventValue: string,
    screenName: string,
    saveToLocal = false,
    extraData?: AnalyticsExtra,
  ) {
    // communicateWithNative.getAnalyticEventsFromRN(eventName, eventValue, screenName, saveToLocal)
    communicateWithNative.getAnalyticEventsFromRN(eventName, eventValue, screenName, saveToLocal, extraData)
  }

  sendAnalyticsEventsScreenNameToNative(screenName: string) {
    communicateWithNative.getAnalyticEventsFromRNForScreenView(screenName)
  }

  sendAnalyticsLocationDataEventsToNative({
    eventValue,
    screenName,
    locationName,
    latitude,
    longitude,
  }: LocationDataEventType) {
    communicateWithNative.sendLocationDataAnalytics(eventValue, screenName, locationName, latitude, longitude)
  }

  sendAnalyticsPopupEvent(screenName: string, popUpName: string, eventValue: string) {
    communicateWithNative.getAnalyticLogPopUpEvent(screenName, popUpName, eventValue)
  }

  sendAnalyticsPopupOpenEvent(screenName: string, popUpName: string) {
    communicateWithNative.getAnalyticLogOpenPopUpEvent(screenName, popUpName)
  }

  sendAnalyticsPopupCloseEvent(screenName: string, popUpName: string) {
    communicateWithNative.getAnalyticLogClosePopUpEvent(screenName, popUpName)
  }

  logNative(tag: string, message: string) {
    communicateWithNative?.logNative?.(tag, message)
  }

  finishActivity() {
    if (Platform.OS === 'android') {
      communicateWithNative.finishActivity()
    }
  }
  /**
   * Check if a screen is pushed by native
   */
  isCallFromNative(route: RouteProp<any, any>) {
    console.log('Navi Service', this.toScreen, route.name)
    return this.toScreen === route.name
  }

  handleResetHome() {
    communicateWithNative.handleResetHome()
  }

  adjustRecenterNative(bottomPanelHeight: number, bottomSheetIndex: number, fromScreen: string) {
    try {
      communicateWithNative.adjustRecenter(bottomPanelHeight, bottomSheetIndex, fromScreen)
    } catch (e) {
      console.log('adjustRecenterNative', e)
    }
  }

  popToMapLanding() {
    if (Platform.OS === 'ios') communicateWithNative.popToMapLanding(NavigationDataService.rootTag)
    else communicateWithNative.popToMapLanding()
  }
}

const NavigationDataService = new NavigationDataServiceImpl()
export default NavigationDataService

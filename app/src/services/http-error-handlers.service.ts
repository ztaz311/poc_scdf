import { AxiosError } from 'axios'
import { CarDetailsResponse } from './user.service'
export interface ShareCollectionErrorCode {
  error_code?:
    | 'LIMIT_REACHED'
    | 'CONFLICT_COLLECTION_NAME'
    | 'UNAUTHORIZED_COLLECTION'
    | 'COLLECTION_NOT_FOUND'
    | 'API_FAILED'
    | 'BOOKMARK_LIMIT_REACHED'
    | 'UNAUTHORIZED_OPERATION'
    | 'STOPPED_SHARING'
    | 'COLLECTION_DELETED'
    | 'INVALID_SHARE_COLLECTION_TYPE' // can add other codes here when needed
  message: string
}
export const canHandleErrorAtSource = (error: AxiosError<CarDetailsResponse | ShareCollectionErrorCode>) => {
  if (isIUNumberDuplicate(error)) return true
  if (isError(error)) return true
  // other specific conditions for handling at source can be added here.
  return false
}

// specific error to be handled at source
const isIUNumberDuplicate = (error: AxiosError<CarDetailsResponse>) => {
  const responseData = error?.response?.data
  return (
    responseData?.code === 'DUPLICATE_IU_NUMBER' ||
    responseData?.code === 'IU_MAX_SAVE_COUNT_REACHED' ||
    responseData?.code === 'IU_MAX_USER_COUNT_REACHED'
  )
}
export const isError = (error: AxiosError<ShareCollectionErrorCode>) => {
  const errorResponseData = error?.response?.data
  return (
    errorResponseData?.error_code === 'LIMIT_REACHED' ||
    errorResponseData?.error_code === 'CONFLICT_COLLECTION_NAME' ||
    errorResponseData?.error_code === 'COLLECTION_NOT_FOUND' ||
    errorResponseData?.error_code === 'UNAUTHORIZED_COLLECTION' ||
    errorResponseData?.error_code === 'API_FAILED' ||
    errorResponseData?.error_code === 'BOOKMARK_LIMIT_REACHED' ||
    errorResponseData?.error_code === 'STOPPED_SHARING' ||
    errorResponseData?.error_code === 'COLLECTION_DELETED' ||
    errorResponseData?.error_code === 'INVALID_SHARE_COLLECTION_TYPE'
  )
}

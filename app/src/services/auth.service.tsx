import jwtDecode, { JwtPayload } from 'jwt-decode'
import { NativeModules } from 'react-native'

const RNAuth = NativeModules.RNAuth

export interface AppJwtPayload extends JwtPayload {
  'custom:guest_mode': string
  sub: string
}

class AuthServiceImpl {
  idToken: string | undefined

  constructor() {
    this.idToken = undefined
  }

  setIdToken(token?: string) {
    if (!token) {
      return
    }
    this.idToken = token
  }

  async generateNewToken() {
    const newToken = await this.refreshToken()
    this.setIdToken(newToken)
    return newToken
  }

  async getIdToken(): Promise<string> {
    if (!this.idToken) {
      return this.generateNewToken()
    }
    const tokenValid = this.validateIdToken()
    // console.log('TOKEN VALID', tokenValid)
    if (!tokenValid) {
      return this.generateNewToken()
    }

    return this.idToken
  }

  get uniqueKey() {
    if (!this.idToken) {
      return ''
    }
    const tokenPayload: AppJwtPayload = jwtDecode(this.idToken)
    return tokenPayload.sub
  }

  get isGuestMode(): boolean {
    if (!this.idToken) {
      return true
    }
    const tokenPayload: AppJwtPayload = jwtDecode(this.idToken)
    return !!tokenPayload['custom:guest_mode']
  }

  validateIdToken = (): boolean => {
    if (!this.idToken) {
      return false
    }
    const tokenPayload: JwtPayload = jwtDecode(this.idToken)

    const expire = tokenPayload?.exp && tokenPayload?.exp * 1000
    const now = Date.now()
    if (expire && expire <= now) {
      console.log('IDTOKEN expire', expire)
      console.log('current', now)
      return false
    }

    return true
  }

  refreshToken = async (): Promise<string> => {
    console.log('REFRESH')
    return RNAuth.refreshIdToken()
  }
}

const AuthService = new AuthServiceImpl()

export default AuthService

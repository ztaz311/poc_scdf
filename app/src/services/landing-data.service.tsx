import { makeObservable, observable, runInAction } from 'mobx'
import { makePersistable } from 'mobx-persist-store'
import { NativeModules } from 'react-native'
import { LandingModesEnum } from '../enums/landing-modes.enum'
import { appStorageController } from '../utils/appStorageController'

const persistKey = 'LandingDataAsyncStorageKey'
const communicateWithNative = NativeModules.CommunicateWithNative

export type Selection = 'HOME' | 'WORK' | 'SKIP'

class LandingDataServiceImpl {
  bottomSheetIndex = 1
  isBottomSheetScrolled = false
  isShowBottomBar = true
  activeTab: LandingModesEnum = LandingModesEnum.NearbyAmenities
  searchValue = ''
  constructor() {
    makeObservable(
      this,
      {
        activeTab: observable,
        bottomSheetIndex: observable,
        isBottomSheetScrolled: observable,
        isShowBottomBar: observable,
        searchValue: observable,
      },
      { autoBind: true },
    )

    makePersistable(
      this,
      {
        name: persistKey,
        storage: appStorageController,
        properties: [],
      },
      { delay: 50, fireImmediately: false },
    )
  }

  clearAllData() {
    this.activeTab = LandingModesEnum.NearbyAmenities
  }

  changeActiveTab(value: LandingModesEnum, optionalParams?: any) {
    runInAction(() => {
      this.activeTab = value
    })
    // communicateWithNative.switchMode(value)
  }

  setBottomSheetScrollState(isScrolling: boolean) {
    runInAction(() => {
      this.isBottomSheetScrolled = isScrolling
    })
  }

  setShowBottomBar(isShow: boolean) {
    runInAction(() => {
      this.isShowBottomBar = isShow
    })
  }

  updateBottomSheetIndex(index: number) {
    runInAction(() => {
      this.bottomSheetIndex = index
      if (this.bottomSheetIndex !== 2) {
        this.isBottomSheetScrolled = false
      }
    })
  }
  updateSearchValue(value: string) {
    runInAction(() => {
      this.searchValue = value
    })
  }
}

const LandingDataService = new LandingDataServiceImpl()
export default LandingDataService

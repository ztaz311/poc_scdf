import { makeObservable, observable, runInAction } from 'mobx'
import { makePersistable } from 'mobx-persist-store'
import { reactScreens } from '../constants/screensName'
import { NotificationType } from '../models/inbox-response'
import { appStorageController } from '../utils/appStorageController'

const persistKey = 'InboxMessagesAsyncStorageKey'

export const INBOXES_PER_PAGE = 25

export enum HandledNavigationScreens {
  EXPLORE = 'Explore',
  VOUCHERS = 'Vouchers',
  PARKING_CALCULATOR = 'ParkingCalculator',
  AMENITIES = 'Amenities',
  MORE_TAB = 'More',
}
export interface NavigationData {
  screenName: string
  navigationParams?: {
    [param: string]: any
  }
  screenType: 'native' | 'react-native'
}
export interface AnalyticsData {
  eventName: string
  eventValue: string
  screenName: string
}
export interface CustomWebviewNavData {
  data: NavigationData
  type: 'navigation'
}
export interface CustomWebviewAnalyticsData {
  data: AnalyticsData
  type: 'analytics'
}
export interface WebLinkNavigationEvent {
  data: {
    type: 'external_browser' | 'in_app_browser'
    url: string
  }
  type: 'open_link'
}

export type CustomWebviewEvent = CustomWebviewNavData | CustomWebviewAnalyticsData | WebLinkNavigationEvent

class InboxMessagesServiceImpl {
  messageList: NotificationType[] = []
  seenMessages: number[] = []
  hasUnreadNotification = false
  currentPage = 1

  constructor() {
    makeObservable(
      this,
      {
        messageList: observable,
        seenMessages: observable,
        hasUnreadNotification: observable,
      },
      { autoBind: true },
    )

    makePersistable(
      this,
      {
        name: persistKey,
        storage: appStorageController,
        properties: ['seenMessages'],
      },
      { delay: 50, fireImmediately: false },
    )
  }

  fetchMessageList() {
    // return HttpService.get(`/user/messagesv3?page=${this.currentPage}`, {})
    //   .then(res => res.data)
    //   .then((data: InboxListResponseType) => {
    //     runInAction(() => {
    //       this.messageList =
    //         this.currentPage > 1
    //           ? [...this.messageList, ...data.notifications].map(item => ({
    //               ...item,
    //               seen: this.seenMessages.indexOf(item.notificationId) >= 0,
    //             }))
    //           : data.notifications.map(item => ({
    //               ...item,
    //               seen: this.seenMessages.indexOf(item.notificationId) >= 0,
    //             }))
    //       this.hasUnreadNotification = this.messageList.filter(item => !item.seen).length > 0
    //     })
    //     return this.messageList
    //   })
    //   .catch(e => console.log(e))
  }

  updateSeenList(notificationId: number) {
    runInAction(() => {
      this.seenMessages =
        this.seenMessages.indexOf(notificationId) >= 0 ? [...this.seenMessages] : [...this.seenMessages, notificationId]

      this.messageList = this.messageList.map(item => ({
        ...item,
        seen: this.seenMessages.indexOf(item.notificationId) >= 0,
      }))
      this.hasUnreadNotification = this.messageList.filter(item => !item.seen).length > 0
    })
  }

  async refresh() {
    this.currentPage = 1
    await this.fetchMessageList()
  }

  loadMoreData() {
    runInAction(() => {
      this.currentPage += 1
      this.fetchMessageList()
    })
  }

  handleInboxNavigation(navigationData: NavigationData, navigation: any) {
    switch (navigationData.screenName) {
      // case HandledNavigationScreens.EXPLORE:
      //   NavigationDataService.openNativeScreen(
      //     nativeScreens.Explore,
      //     { data: JSON.stringify(navigationData.navigationParams) } || undefined,
      //   )
      //   break

      default:
        navigation.navigate(reactScreens.Home)
        break
    }
  }
}

const InboxMessagesService = new InboxMessagesServiceImpl()
export default InboxMessagesService

import { AlertButton, Platform } from 'react-native'
import { CustomAlert } from '../components/AlertAndroid'
import AlertIOS from '../components/NativeModules/AlertIOS'

type RefObject = {
  screen: string
  ref: CustomAlert
  screenID: string
}
class AlertServiceImpl {
  private refs: RefObject[] = []

  get currentRef() {
    if (this.refs.length > 0) return this.refs[this.refs.length - 1].ref
    return undefined
  }

  removeRef(screen: string, id: string) {
    this.refs = this.refs.filter(ref => !(ref.screen === screen && ref.screenID === id))
  }

  addRef(screen: string, ref: CustomAlert, id: string) {
    const existingRefIndex = this.refs.findIndex(object => object.screen === screen && object.screenID === id)
    if (existingRefIndex > -1) {
      this.refs[existingRefIndex].ref = ref
    } else {
      this.refs.push({ screen, ref, screenID: id })
    }
  }

  alert(title: string, message?: string, buttons: AlertButton[] = [{ text: 'Okay' }]) {
    if (Platform.OS === 'android') {
      this.currentRef?.show(title, message, buttons)
      // AlertAndroid.alert(title, message, buttons, {cancelable: false});
    } else {
      AlertIOS.alert(title, message, buttons)
    }
  }
}

const AlertService = new AlertServiceImpl()
export default AlertService

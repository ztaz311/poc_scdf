import { DefaultTheme } from '@react-navigation/native'
import { useEffect } from 'react'
import { NativeModules } from 'react-native'
import { createCustomStackNavigator } from '../components/Navigator/CustomStackNavigator'
import { ParamsFromNative } from '../services/navigation-data.service'

const communicateWithNative = NativeModules.CommunicateWithNative

const Stack = createCustomStackNavigator()

type RouteProps = {
  initialRouteName?: string
  navigationParams?: ParamsFromNative
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#0000',
  },
}

// const FULL_SCREEN_LIST = [
//   hybridScreens.DateRangePicker,
//   hybridScreens.UserPreference,
//   hybridScreens.ETAScreen,
//   hybridScreens.FavouriteAddNew,
//   hybridScreens.FavouriteList,
//   hybridScreens.ETAFavouriteList,
//   hybridScreens.SearchLocation,
// ]

const HybridRouter = ({ initialRouteName, navigationParams }: RouteProps) => {
  useEffect(() => {
    // if (Platform.OS === 'ios') {
    //   communicateWithNative.changeRNScreen(FULL_SCREEN_LIST.includes(initialRouteName as string) ? true : false)
    // }
  }, [initialRouteName, navigationParams])

  return null

  // return (
  //   <NavigationContainer
  //     theme={MyTheme}
  //     ref={navigationHybridRef}
  //     onReady={() => {
  //       isNavigationReadyHybrid.current = true
  //     }}>
  //     <Stack.Navigator initialRouteName={initialRouteName}>
  //       {/* <Stack.Screen
  //         name={hybridScreens.ParkingPriceDetail}
  //         key={hybridScreens.ParkingPriceDetail}
  //         component={homePanelScreen}
  //         initialParams={navigationParams}
  //       /> */}
  //     </Stack.Navigator>
  //   </NavigationContainer>
  // )
}

export default HybridRouter

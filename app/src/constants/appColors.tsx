import { StyleSheet } from 'react-native'

const _privateStyleDict = {
  navigationBarLineColor: '#ECEDEC',
}

export const lightColors = {
  transparent: 'rgba(0, 0, 0, 0)',
  primaryBackground: '#FCFCFC',
  primaryBackground2: '#FCFCFC',
  primaryGray: '#EBEBEB',
  primaryText: '#222638',
  primaryText2: '#222638',
  primaryColor: '#782EB1',
  secondaryText: '#778188',
  redPink: '#E82370',
  inactiveColor: '#C1C1C1',
  white: '#fff',
  greyText: '#778188',
  homeTabLabel: '#778188',
  homeTabBackground: '#fff',
  homeTabBackgroundActived: '#782EB1',
  homeTabBorder: '#C1C1C1',
  amenitiesBackground: '#e2e2e2',
  amenityListBackground: '#F2F2F2',
  amenityItemBackground: '#F6F7F8',
  selectedAmenityText: '#222638',
  expressWayNotationText: '#782EB1',
  expressWayNotationBackground: 'rgba(193, 144, 224, 0.2)',
  amenityText: '#778188',
  inputBackground: ' rgba(255, 255, 255, 0.45)',
  divider: '#C9C9CE',
  activeERP: '#F7EDFF',
  backButtonBackground: '#E4D5EF',
  calculatorBackground: '#F5F5F5',
  freeTextColor: '#15B765',
  seasonTextColor: '#E82370',
  datePickerBackground: '#FCFCFC',
  calculatorTimePickerSelected: '#F7EDFF',
  dayTabColor: '#778188',
  dayTabBGColor: '#FFFFFF',
  activeDayTabColor: '#FFFFFF',
  activeDayTabBGColor: '#933DD8',
  selectedItem: '#F5EEFC',
  inputModal: '#EBEBEB',
  borderBottomColor: '#999999',
  textColorForDark: '#222638',
  backIconBgColor: '#E4D5EF',
  backgroundColorModal: '#FCFCFC',
  borderBottomItemColor: 'rgba(60, 60, 67, 0.360784)',
  vehicleIUNumber: '#FFFFFF',
  borderTagVoucher: '#BCBFC4',
  readyToRedeem: '#EDFDF5',
  readyToRedeemColor: '#15B765',
  headerTable: '#F5F5F5',
  tableSelected: '#F7EDFF',
  backgroundCheapest: '#EDFDF5',
  thumbColorInactive: '#FFFFFF',
  trackColorInactive: '#C4C4C4',
  descriptionExploreMap: '#222638',
  headerCarpark: '#f1f1f1',
  txtLabe: '#f1f1f1',
  arrowCarpark: '#FCFCFC',
  bgShowTime: '#FFFFFF',
  txtShowTime: '#222638',
  noCarparkBg: '#F5EEFC',
  noCarparkColor: '#BCBFC3',
  borderHeader: '#000',
  textDisabled: '#8D8B9B',
  borderBottomDash: '#E2E2E2',
  greenCardLeftBG: '#F5F5F5',
}

export const Colors = lightColors

export const darkColors = {
  primaryBackground: '#222638',
  primaryBackground2: '#393F58',
  primaryGray: '#787E98',
  primaryText: '#FCFCFC',
  primaryText2: '#FFFFFF',
  secondaryText: '#BFC1C9',
  primaryColor: '#B155F8',
  inactiveColor: '#787e98',
  redPink: '#EA347A',
  white: '#fff',
  homeTabLabel: '#fff',
  homeTabBackground: '#393F58',
  homeTabBackgroundActived: '#B155F8',
  homeTabBorder: '#4F5675',
  greyText: '#DDDDDD',
  expressWayNotationText: '#FCFCFC',
  expressWayNotationBackground: '#B155F8',
  amenitiesBackground: '#393F58',
  amenityListBackground: '#393F58',
  amenityItemBackground: '#4F5675',
  selectedAmenityText: '#FCFCFC',
  amenityText: '#C1C1C1',
  inputBackground: '#782EB1',
  divider: '#4F5778',
  activeERP: '#B155F8',
  backButtonBackground: '#782EB1',
  calculatorBackground: '#393F58',
  freeTextColor: '#15B765',
  seasonTextColor: '#EA347A',
  datePickerBackground: '#393F58',
  calculatorTimePickerSelected: 'rgba(77, 52, 114, 1)',
  dayTabColor: '#C1C1C1',
  dayTabBGColor: '#222638',
  activeDayTabColor: '#FFFFFF',
  activeDayTabBGColor: '#B155F8',
  selectedItem: '#390561',
  inputModal: '#393F58',
  borderBottomColor: '#000000',
  textColorForDark: '#fff',
  backIconBgColor: '#B155F8',
  backgroundColorModal: '#393F58',
  borderBottomItemColor: '#4F5778',
  vehicleIUNumber: '#2C3146',
  borderTagVoucher: '#787E98',
  readyToRedeem: '#0B5B33',
  readyToRedeemColor: '#18C970',
  headerTable: '#393F58',
  tableSelected: '#390561',
  backgroundCheapest: '#0B5B33',
  thumbColorInactive: '#787E98',
  trackColorInactive: '#393F58',
  descriptionExploreMap: '#BFC1C9',
  headerCarpark: '#393F58',
  arrowCarpark: '#C2C0CD',
  bgShowTime: '#222638',
  txtShowTime: '#FFFFFF',
  noCarparkBg: '#390561',
  noCarparkColor: '#787E98',
  borderHeader: '#FCFCFC',
  textDisabled: '#C2C0CD',
  borderBottomDash: '#4F5778',
  greenCardLeftBG: '#393F58',
}

export type AppColors = typeof lightColors

export const appColors = StyleSheet.create({
  navigationBarTextColor: {
    color: 'rgb(34,38,56)',
  },

  appBackgroundColor: {
    backgroundColor: 'white',
  },

  appBrandColor: {
    color: 'rgba(120, 46, 177, 1)',
  },
  navigationBarLineColor: {
    backgroundColor: _privateStyleDict.navigationBarLineColor,
  },

  tagHeaderTextColor: {
    color: 'rgba(112,112,112,1.0)',
  },
  contactSeperatorColor: {
    backgroundColor: 'rgba(201,201,206,1.0)',
  },
  viewBgColor: {
    backgroundColor: 'rgba(235,235,235,1.0)',
  },
  contactNameTextColor: {
    color: 'rgba(0,0,0,1.0)',
  },

  etaMsgViewTextColor: {
    color: 'rgba(48,52,74,1.0)',
  },

  checkBoxTextColor: {
    color: 'rgba(17,20,33,1.0)',
  },

  searchBGColor: {
    backgroundColor: 'rgba(235, 235, 235, 1)',
  },

  searchText: {
    color: 'rgba(119, 129, 136, 1)',
  },

  searchTextFill: {
    color: 'rgba(48,52,74,1.0)',
  },
})

export default appColors

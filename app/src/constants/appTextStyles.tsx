import { Platform, StyleSheet } from 'react-native'

export const FontFamily = Object.freeze({
  SFProDisplay: Platform.select({
    android: 'SF-Pro-Display',
    default: 'SF Pro Display',
  }),
  Quicksand: Platform.select({
    android: 'Quicksand',
    default: 'Quicksand',
  }),
})

export const FontFamilyOld = Object.freeze({
  SFProDisplayRegular: Platform.select({
    android: 'SF-Pro-Display-Regular',
    default: 'SF-Pro-Display-Regular',
  }),
  SFProDisplayMedium: Platform.select({
    android: 'SF-Pro-Display-Medium',
    default: 'SF-Pro-Display-Medium',
  }),
  SFProDisplayBold: Platform.select({
    android: 'SF-Pro-Display-Bold',
    default: 'SF-Pro-Display-Bold',
  }),
  QuicksandBold: Platform.select({
    android: 'Quicksand-Bold',
    default: 'Quicksand-Bold',
  }),
})

export const appTextStyles = StyleSheet.create({
  default: {
    fontFamily: FontFamily.SFProDisplay,
    color: '#111421',
  },
})

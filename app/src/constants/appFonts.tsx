import { StyleSheet } from 'react-native'

export const appFonts = StyleSheet.create({
  navigationBarTitle: {
    fontSize: 22,
    fontWeight: '500',
  },
  tagHeaderFont: {
    fontSize: 20,
    fontWeight: '400',
  },
  contactNameFont: {
    fontSize: 20,
    fontWeight: '400',
  },
  etaMsgFont: {
    fontSize: 20,
    fontWeight: '400',
  },
  etaCheckBoxFont: {
    fontSize: 20,
    fontWeight: '400',
  },
  nextBtnFont: {
    fontSize: 20,
    fontWeight: '600',
  },
  searchTxtFont: {
    fontSize: 20,
    fontWeight: '400',
  },
})

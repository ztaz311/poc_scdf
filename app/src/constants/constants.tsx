import { Dimensions, Platform } from 'react-native'

export const PANEL_HEIGHT = 430
export const WIDTH_IP_SE = 375

export const deviceWidth = Dimensions.get('screen').width
export const deviceHeight = Dimensions.get('screen').height
export const scaleWidth = deviceWidth / 414 // 414 get from design
export const scaleHeight = deviceWidth / 896 // 414 get from design

export const DARK_THEME_KEY = 'IS_DARK_THEME'

export const bottomTabsHeight = Platform.OS === 'ios' ? (deviceWidth > WIDTH_IP_SE ? 90 : 70) : 54

export const hitSlop = { top: 16, bottom: 16, left: 16, right: 16 }

export const HOURS_A_DAY = 24

export const EXPIRING_HOURS = 1

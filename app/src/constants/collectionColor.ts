import { DarkImages, Images } from './appImages'

export const collectionColors = ['#782EB1', '#F26415', '#15B765', '#E82370']

export const collectionImages = [Images.savedOrange, Images.savedViolet, Images.savedGreen, Images.savedPink]
export const collectionImagesDarkMode = [
  DarkImages.savedOrange,
  DarkImages.savedViolet,
  DarkImages.savedGreen,
  DarkImages.savedPink,
]

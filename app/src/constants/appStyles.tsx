'use-strict'

import { StyleSheet } from 'react-native'
import appColors from './appColors'

export const appStyles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: appColors.viewBgColor.backgroundColor,
  },
})

export default appStyles

import AsyncStorage from '@react-native-async-storage/async-storage'
import React, { createContext, FC, ReactNode, useContext, useEffect, useState } from 'react'
import { Platform } from 'react-native'
import { AppColors, darkColors, lightColors } from '../constants/appColors'
import { AppImages, DarkImages, Images } from '../constants/appImages'
import { DARK_THEME_KEY } from '../constants/constants'

type AppTheme = {
  isDarkMode: boolean
  themeColors: AppColors
  themeImages: AppImages
  setTheme: (value: boolean) => void
}

export const AppThemeContext = createContext<AppTheme>({
  isDarkMode: false,
  themeColors: lightColors,
  themeImages: Images,
  setTheme: () => {},
})

type Props = {
  isDarkMode: boolean
  children: ReactNode
}

export const AppThemeProvider: FC<Props> = ({ isDarkMode, children }) => {
  const [isDarkTheme, setIsDarkTheme] = useState<boolean>(isDarkMode)

  useEffect(() => {
    if (Platform.OS === 'ios') {
      AsyncStorage.getItem(DARK_THEME_KEY, (error?: any, result?: string | null) => {
        setIsDarkTheme(result === 'true' ? true : false)
      })
    }
  }, [])

  const defaultTheme = {
    isDarkMode: isDarkTheme,
    themeColors: isDarkTheme ? darkColors : lightColors,
    themeImages: isDarkTheme ? DarkImages : Images,
    setTheme: setIsDarkTheme,
  }

  return <AppThemeContext.Provider value={defaultTheme}>{children}</AppThemeContext.Provider>
}

// Custom hook to get the theme object returns {isDark, colors, setScheme}
export const useTheme = () => useContext(AppThemeContext)

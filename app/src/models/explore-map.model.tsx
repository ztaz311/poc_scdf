export interface CustomData {
  statusCode: number
  content_details: CustomContent[]
  content_category: CustomCategory[]
}
export interface CustomCategory {
  content_categories_id: number
  name: string
  content_title: string
  content_description: string
  content_ids: CustomContentID[]
}
export interface CustomContentID {
  content_id: number
  default_selection: number
  sequence_order: number
}
export interface CustomContent {
  // content_id: string
  content_id: number
  name: string
  layer_code: string
  is_new_layer: boolean
  recentre_zone_radius: number
  description: string
  sequence_order: number
  default_selection: number
  image_url_light: string
  image_url_dark: string
  content_categories_id?: number
  type: 'ZONE' | 'GLOBAL_POIS'
  enable_zone_drive: boolean
  track_navigation: boolean
  enable_walking?: number
  enableWalking: boolean
}

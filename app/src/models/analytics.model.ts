type InboxDetailAnalytics = {
  link_value?: string
  message?: string
}

/**
 * For other events, other @type should be defined and use format like this:
 *
 * type AnalyticsExtra = TypeOne | TypeTwo | TypeThree
 */
export type AnalyticsExtra = InboxDetailAnalytics

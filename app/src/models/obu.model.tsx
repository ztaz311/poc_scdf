import { OBUCardInfoTypeEnum, OBUEvenTypeEnum, OBUStatusPairingEnum } from '../enums/obu.enum'
import { FuelGradeType, PowerType, VehicleType } from '../screens/obu-settings/obu-settings.enum'

export type OBUEventNotifyType = {
  type: OBUEvenTypeEnum
  message?: string
  distance?: string
  detailTitle?: string
  detailMessage?: string
  chargeAmount?: string
}

export type OBUCashCardInfoType = {
  status: OBUCardInfoTypeEnum
  balance?: number | string
  paymentMode?: 'frontend' | 'backend' | 'businessFunctionDisabled'
}

export type OBULiteSpeedType = {
  speed: number
  limitSpeed: number
}

export type OBUStatusPairType = {
  paymentMode?: 'frontend' | 'backend' | 'businessFunctionDisabled'
  status: OBUStatusPairingEnum
  cardBalance?: number
  obuName?: string
  vehicleNumber?: string
}

export type OBUDetailType = {
  paymentMode?: 'frontend' | 'backend' | 'businessFunctionDisabled'
  cardBalance?: number | string | undefined
  obuName?: string
  vehicleNumber?: string
  vehicleName?: string
  cognitoID?: string
}

export type OBUCarparkDetailType = {
  name: string
  availableLots: number | string
  color: string
}

export type BusLaneRangeType = {
  content: string
  endTime: string
  startTime: string
}
export type BusLaneDataType = {
  busLaneIcon: string
  busLaneType: 'FULL' | 'NORMAL'
  busLaneTitle: string
  includedDaysOfWeek: number[]
  timeRange: BusLaneRangeType[]
  isShow?: boolean
}
export type OBUBusLaneDataType = {
  busLaneDisplayEnabled: boolean
  data: BusLaneDataType[]
}

export type VehicleItemNative = {
  vehicleNumber: string
  obuName: string
  vehicleType?: VehicleType
  paymentMode?: 'frontend' | 'backend' | 'businessFunctionDisabled'
}

export type PairedVehicleListNumber = string[]

export interface PairedVehicleData {
  vehicleNumber: string
  vehicleType: VehicleType
  powerType: PowerType
  price: number
  fuelGrade: FuelGradeType
  consumptionPerKm: number
  obuName?: string
  vehicleName?: string
  paymentMode?: 'frontend' | 'backend' | 'businessFunctionDisabled'
  isModified?: boolean
  vehicleModelId?: number | string
  make?: string
  model?: string
}

export type FilterVehicleDataRequest = {
  vehicleNumbers: PairedVehicleListNumber
}

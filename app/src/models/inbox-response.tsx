export type InboxListResponseType = {
  currentPage: number
  notifications: NotificationType[]
}

export type WelcomeNotiVideo = {
  title: string
  video_url: string
  thumbnail_url: string
  weblink: string
}

export type TutorialExtraData = {
  videos: WelcomeNotiVideo[]
  description: string[]
}

export type NotificationType = {
  notificationId: number
  priority: number
  category: NotificationCategoryEnum
  description: string
  expireTime: number
  imageLinkDark: string
  imageLinkLight: string
  latitude?: number
  longitude?: number
  message: string
  short_name: string
  startTime: number
  title: string
  type: string
  seen?: boolean
}

export enum NotificationCategoryEnum {
  ALL = 'ALL',
  ADMIN = 'ADMIN',
  TRAFFIC_ALERTS = 'TRAFFIC_ALERTS',
  UPCOMING_TRIP = 'UPCOMING_TRIP',
  TUTORIAL = 'TUTORIAL',
  VOUCHER = 'VOUCHER',
  CUSTOM_INBOX = 'CUSTOM_INBOX',
}

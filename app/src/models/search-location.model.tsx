import { ShortcutCode } from './collection.model'

export enum SearchLocationType {
  favourite = 'B',
  recent = 'H',
  searchAPI = 'S',
}

export enum PinType {
  ENTRY = 'entry',
  EXIT = 'exit',
  BICYCLE_STAND = 'bicycle-stand',
  PICKUP_POINT = 'pickup-point',
  TAXI_STAND = 'taxi-stand',
  LOADING_BAY = 'loading-bay',
  ENTRANCE = 'entrance',
}

export type PinItem = {
  carparkId: string
  icon: any
  lat: string
  long: string
  name: string
  type: PinType
}

export type SearchLocationModel = {
  carparkId?: string
  placeId?: string
  addressid?: number
  lat?: string
  long?: string
  name?: string
  address1?: string
  address2?: string
  fullAddress?: string
  time?: string
  distance?: string // e.g: 1.0 KM
  type?: SearchLocationType
  index?: number // array index
  source?: string // returned from response of API
  token?: string // returned from response of API
  alias?: string
  isBookmarked: boolean
  bookmarkId?: number
  code: ShortcutCode
  selectedPinIndex?: number
  routablePoints?: PinItem[]
  description?: string
  availabilityCSData?: any
  availablePercentImageBand?: string
  userLocationLinkIdRef?: number | null
  showAvailabilityFB?: boolean
  hasAvailabilityCS?: boolean
}

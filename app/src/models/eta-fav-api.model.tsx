type ETAFavouriteApiModel = {
  tripEtaFavouriteId?: number
  recipientName: string
  recipientNumber: string
  shareLiveLocation: string
  voiceCallToRecipient: string
  destination: string
  tripDestLat: string
  tripDestLong: string
  message: string
  module?: string
  count?: number
  addressid?: string
  alias?: string
  name?: string
}

export type ETAFavouriteSendAPIModel = {
  recipientName: string
  recipientNumber: string
  shareLiveLocation: string
  voiceCallToRecipient: string
  destination: string
  tripDestLat: string
  tripDestLong: string
  message: string
}

export type ETADetails = {
  contactName: string
  contactNumber: string
  shareLiveLoc: boolean
  sendVoice: boolean
  etaMessage: string
}

export interface ETAFavListGetResponse {
  favourites: ETAFavouriteApiModel[]
}

export default ETAFavouriteApiModel

export type DetailRow = {
  name: string
  value: string | string[]
}

export type SubAmenity = {
  type: string
  title: string
  imageLink: string
  details: string
  order: number
  displayType: 'TABLE' | 'LIST'
  tableDetails: DetailRow[]
  listDetails: string[]
}

export type CarparkImage = {
  category: string
  title: string
  description: string
  otherCategory: string
  key: string
  location: string
}

export type AmenityOtherImage = {
  category: string
  title: string
  description: string
  location: string
  type: 'image' | 'webview'
  webview_url?: string
  thumbnail_url: string
}

export type CarparkImages = {
  [key: string]: CarparkImage[]
}

export type CarparkAvailabilities = {
  monday: number[]
  tuesday: number[]
  wednesday: number[]
  thursday: number[]
  friday: number[]
  saturday: number[]
  sunday: number[]
}

export type ArticleCard = {
  thumbnail_url: string
  redirect_url: string
  title: string
  link_text: string
}

export type TBRArticle = {
  header: string
  list: ArticleCard[]
}

export type TBRAmenityDetails = {
  id: string
  name: string
  mainContent?: string
  amenities: SubAmenity[]
  articles: TBRArticle
  images?: CarparkImages
  audioContent: [{ location?: string }]
}

import { FuelGradeType, PowerType, VehicleType } from '../screens/obu-settings/obu-settings.enum'

export type VehicleSettingMake = {
  make: string
}

export type VehicleSettingModel = {
  vehicleModelId: number | string | null
  vehicleType: VehicleType
  make: string
  model: string
  powerType: PowerType | string
  hybrid: 'NO' | 'YES'
  price: number
  consumptionPerKm: number
}

export type VehicleSettingOBUUser = {
  vehicleNumber: string
  vehicleModelId: number | string | null
  make?: string
  model: string
  fuelGrade?: FuelGradeType | null
  vehicleType?: VehicleType
  powerType: PowerType | string
  price?: number | string
  consumptionPerKm?: number
}

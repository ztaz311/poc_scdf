export type ERPItemType = {
  erpid: number
  endlat: string
  endlong: string
  name: string
  startlat: string
  startlong: string
  zoneid: string
}

export type ERPRateType = {
  zoneid: string
  rates: RateType[]
}

export type RateType = {
  chargeamount: number
  daytype: string
  effectivedate: string
  endtime: string
  starttime: string
  vehicletype: string
}

export type PublicHolidayType = {
  chargeamount: number
  endtime: string
  starttime: string
  date: string
}

import { TextStyle, ViewStyle } from 'react-native'

export type RemoteViewStyle = {
  dark?: ViewStyle
  light?: ViewStyle
}

export type RemoteTextStyle = {
  dark?: TextStyle
  light?: TextStyle
}

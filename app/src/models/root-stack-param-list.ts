import { LocationTypesEnum } from '../enums/location-types-enum'
import { ParkingInfoEnum } from '../enums/parking-info.enum'
import { SearchFromScreensEnum } from '../enums/search-from-screens.enum'
import { TravelTimeData } from '../screens/landing/home/obu/components/ContentTravelTime.component'
import { AndroidOpenAs } from '../services/navigation-data.service'
import { BookmarkLocation, ShortcutCode } from './collection.model'
import { NotificationType } from './inbox-response'
import { OBUCarparkDetailType } from './obu.model'
import { ParkingAvailableUpdateStatus } from './parking-available.model'
import { ParkingPriceDetails } from './parking-price.model'
import { SearchLocationModel } from './search-location.model'
import { UserOnboardingState } from './user-preference-api.model'
import { VoucherType } from './voucher-response.model'

export type RootStackParamList = {
  Home: {
    isNotLoad?: boolean
  }
  Notify: undefined
  TripLog: undefined
  More: undefined
  SearchLocation: {
    etaMode?: string
    fromScreen?: string
    locationType?: LocationTypesEnum
  }
  RNSearchLocation: {
    etaMode?: string
    fromScreen: SearchFromScreensEnum
    locationType: LocationTypesEnum
  }
  ParkingPriceDetail: {
    parkingId: string
    onBackAndroid?: () => void
  }
  ParkingRateDetail: {
    details: ParkingPriceDetails
  }
  TBRAmenityDetails: {
    id: string
  }
  FavouriteList: undefined
  FavouriteAddNew: undefined
  CarDetailScreen: undefined
  ETAScreen: {
    contactname?: string
    contactnumber?: string
    destination?: string
    etaMode?: string
    etaid?: number
    etamessage?: string
    etastatus?: string
    lat?: string
    long?: string
    sendvoice?: boolean
    shareliveloc?: boolean
  }
  Contacts: undefined
  ETAFavouriteList: {
    etaMode: string
    etaMessage: string
  }
  FavouriteEditInfo: {
    location: SearchLocationModel
  }
  UserPreference: {
    onboardingState: UserOnboardingState
  }
  DateRangePicker: {
    start_date: string | null
    end_date: string | null
    disable_future_dates?: boolean
  }
  NearbyParkingFees: {
    title?: string
    isOpenFromNative: boolean
  }
  ParkingCalculator: {
    start_timestamp?: number
    parkingID?: string
    fromNative?: boolean
    openAs?: AndroidOpenAs
    lat?: string
    long?: string
  }
  ERP: {
    erpId?: number
    erpSelTS?: number
  }
  ERPDetail: {
    erpId?: number
    erpSelTS?: number
    showPlanTrip?: boolean
  }
  ExpressWayDetail: {
    zoneID?: string
  }
  Inbox?: {
    notificationData?: NotificationType
  }
  Tutorial: undefined
  VoucherDetails: {
    voucher: VoucherType
  }
  Voucher: undefined // Bottom tab voucher list screen
  CarDetails: undefined
  PreferenceSettings: undefined
  PetrolPreference: undefined
  EVChargerPreference: undefined
  MapDisplayPreference: undefined
  ParkingPreference: undefined
  CollectionList: undefined
  ShortcutListFull: undefined
  Explore: undefined
  PoiDetails: undefined
  CollectionDetails: {
    collection_id: number
    collection_name: string
    collection_description: string
    code: string
    bookmarkId?: number
  }
  ShortcutAddNew: {
    fromPrompt?: boolean
    shortcutType: ShortcutCode
    bookmarkId?: number
    collectionId?: number
  }
  ChooseCollection: {
    bookmark: BookmarkLocation
    isDropPinLanding?: boolean
    isSaveLocation?: boolean
    isFromInvite?: boolean
    onGoBack?: () => void
    showHeader?: () => void
    fromScreen?: string
  }
  CollectionSearch: {
    onCallBack: (data: SearchLocationModel) => void
    collectionId: number
  }
  BookmarkEditor: {
    bookmark: BookmarkLocation
  }
  WebViewPanel: {
    title: string
    webviewUrl: string
    closeAction?: string // todo: make this an enum of available actions
  }
  ParkingAvailable: {
    data: ParkingAvailableUpdateStatus
    isWalking: boolean
  }
  ParkingAvailableFeedbackComplete: undefined
  CreateAccountPopup: {
    availablePercentImageBand: ParkingInfoEnum
    carparkId: string
  }
  OBUDisplay: undefined
  CarparkList: {
    location?: {
      lat: string
      long: string
    }
  }
  DriveTrendEnergy: {
    isEV: boolean
    data: any
    currentWeek: string
  }
  OBUTravelTime: {
    data: TravelTimeData[]
    fromOBU?: boolean
  }
  OBUCarparkAvailability: {
    carparks: OBUCarparkDetailType[]
    fromOBU?: boolean
  }
  DriveTimeScreen: {
    drivingTime: any
    drivingDistance: any
    currentWeek: string
  }
  VehicleDetailsScreen: {
    vehicleNumber: string
    vehicleName?: string
  }
  TotalERPFeeScreen: {
    erpFee: any
    currentWeek: string
  }
  TotalParkingFeeScreen: {
    parkingFee: any
    currentWeek: string
  }
  ListPairedVehicles: undefined
  PinScreen: undefined
  SharedLocationScreen: {
    sharedLocation: any
  }
  ShareCollectionScreen: {
    collectionToken: string
  }
  LocationInviteScreen: {
    data: {
      address1: string
      address2?: string
      latitude: string
      longitude: string
      name: string
      placeId?: string
      userLocationLinkIdRef?: string | null
    }
    fromScreen: string
  }
}

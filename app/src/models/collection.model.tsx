import { PinItem, SearchLocationModel } from './search-location.model'

export const MAX_SHORTCUT_COUNT = 5
export type ShortcutType = 'normal' | 'home' | 'work'
export type ShortcutCode = 'HOME' | 'WORK' | 'NORMAL' | 'CUSTOM'
export type BookmarkLocation = {
  bookmarkId: number
  lat: string
  long: string
  name?: string
  code: ShortcutCode
  address1: string
  address2: string
  amenityType?: string
  amenityId?: string
  isDestination: boolean
  isSelected?: boolean
  collectionId: number
  description: string
  routablePoints?: PinItem[]
  userLocationLinkId?: string
  fullAddress?: string
  userLocationLinkIdRef?: number | null
  placeId?: string
  availablePercentImageBand?: string
  availabilityCSData?: any
  showAvailabilityFB?: boolean
  hasAvailabilityCS?: boolean
}

export type CollectionCode = 'SAVED_PLACES' | 'SHORTCUTS' | 'NORMAL' | string
export type CollectionModel = {
  code: CollectionCode
  collectionId: number
  count: number
  description: string
  name: string
  pinned: boolean
  image: string
  ownerName?: string
}

export function searchLocationToBookmarkLocation(search: SearchLocationModel) {
  if (search.lat && search.long) {
    const bookmark: BookmarkLocation = {
      bookmarkId: search.bookmarkId,
      lat: search.lat,
      long: search.long,
      name: search.name,
      address1: search.address1 || '',
      address2: search.address2 || '',
      fullAddress: search.fullAddress || '',
      amenityType: '',
      amenityId: '',
      code: search.code,
      isDestination: false,
      collectionId: -1,
      description: '',
      routablePoints: search.routablePoints,
      userLocationLinkIdRef: search?.userLocationLinkIdRef,
      placeId: search?.placeId,
      availablePercentImageBand: search?.availablePercentImageBand,
      availabilityCSData: search?.availabilityCSData,
      hasAvailabilityCS: search?.hasAvailabilityCS,
      showAvailabilityFB: search?.showAvailabilityFB,
    }
    return bookmark
  }
  return undefined
}

export function bookmarkLocationToSearchLocation(bookmark: BookmarkLocation): SearchLocationModel {
  return {
    addressid: bookmark.bookmarkId,
    lat: bookmark.lat,
    long: bookmark.long,
    name: bookmark.name,
    address1: bookmark.address1,
    address2: bookmark.address2,
    bookmarkId: bookmark.bookmarkId,
    code: bookmark.code,
    isBookmarked: true,
    routablePoints: bookmark?.routablePoints,
    userLocationLinkIdRef: bookmark?.userLocationLinkIdRef,
    placeId: bookmark?.placeId,
    availablePercentImageBand: bookmark?.availablePercentImageBand,
    availabilityCSData: bookmark?.availabilityCSData,
    hasAvailabilityCS: bookmark?.hasAvailabilityCS,
    showAvailabilityFB: bookmark?.showAvailabilityFB,
  }
}

export function isSameLocation(bookmark: BookmarkLocation, searchLocation: SearchLocationModel) {
  return (
    bookmark.lat === searchLocation.lat &&
    bookmark.long === searchLocation.long &&
    bookmark.address1 === searchLocation.address1 &&
    bookmark.address2 === searchLocation.address2
  )
}

export type EditBookmarkResponse = {
  bookmarkId: number
  message: string
}

export type EditBookmarkNativeResponse = {
  result: 'success' | 'fail'
}

export type EditCollectionResponse = {
  collectionId: number
  message: string
}

export type RemoveBookmarkResponse = {
  message: string
}

export type ShareCollection = {
  collectionLinkId: number
  shareMessageTemplate: string
  message: string
}

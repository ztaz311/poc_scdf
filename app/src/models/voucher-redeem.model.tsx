import { VoucherRedeemDetails } from './voucher-response.model'

export type VoucherRedeemPayload = {
  voucherIds: string[]
}

export type VoucherRedeemResponse = {
  code:
    | 'SUCCESS'
    | 'PARTIAL_SUCCESS'
    | 'FAILED'
    | 'API_FAILED'
    | 'FULLY_CLAIMED'
    | 'VOUCHER_ALREADY_USED'
    | 'VEHICLE_DETAILS_NOT_FOUND'
    | 'VOUCHER_NOT_FOUND'
    | 'IU_MAX_USER_COUNT_REACHED'
    | 'IU_MAX_SAVE_COUNT_REACHED'
  message: string
  redeemCode: string
  redeemDetails: VoucherRedeemDetails
}

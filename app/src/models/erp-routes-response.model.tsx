export type ERPRoutesResponse = {
  id: string
  erpRoutes: ERPRoute[]
}

export type ERPRoute = {
  routeId: string
  eRPCharges: ERPCharge[]
}

export type ERPCharge = {
  erpId: number
  chargeAmount: number
}

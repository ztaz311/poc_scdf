import { ParkingInfoEnum } from '../enums/parking-info.enum'

export type ParkingAvailableStatusRequest = {
  availablePercentImageBand: ParkingInfoEnum
  carparkId?: string
  placeId?: string
  userLocationLinkIdRef?: number
  amenityId?: string
  layerCode?: string
}

export type ParkingAvailableUpdateStatus = {
  carparkId?: string
  placeId?: string
  userLocationLinkIdRef?: number
  amenityId?: string
  layerCode?: string
}

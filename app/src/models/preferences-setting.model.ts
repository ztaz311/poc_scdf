import { FuelGradeType, PowerType, VehicleType, YesNoType } from '../screens/obu-settings/obu-settings.enum'

export type PreferenceSettingsAttribute =
  | 'MapDisplay'
  | 'RoutePreference'
  | 'ShowDemo'
  | 'CarparkDistance'
  | 'CarparkCount'
  | 'CarparkAvailability'
  | 'TrafficInformation'
  | 'ParkingAvailabilityDisplay'
  | 'ParkingAvailabilityAudio'
  | 'EstimatedTravelTimeDisplay'
  | 'EstimatedTravelTimeAudio'
  | 'SchoolZoneDisplayAlert'
  | 'SchoolZoneVoiceAlert'
  | 'SilverZoneDisplayAlert'
  | 'SilverZoneVoiceAlert'
  | 'BusLaneDisplayAlert'
  | 'BusLaneVoiceAlert'

export type UserSavedSettingObject = {
  attribute: PreferenceSettingsAttribute
  value?: string // legacy purpose, will be removed and replaced by attribute_code
  attribute_code: string
}

export type UserSavedSettingResponse = {
  settings: UserSavedSettingObject[]
}

export type PreferenceSettingDataFromNative = {
  settings_mapper: string
  settings_user: string
}

export type PreferenceSettingObject<T> = {
  code: T
  value: string
  module: string
  default: string
}

export type MapDisplayCode = 'Light' | 'Dark' | 'Auto'
export type ShowSavedCarparkListViewCode = 'Yes' | 'No'
export type RoutePreferenceCode = 'Fastest' | 'Cheapest' | 'Shortest'
export type ShowDemoCode = 'No' | 'Yes'
export type CarparkDistanceCode = 'METERS_500' | 'METERS_300'
export type CarparkCountCode = 'CP_10' | 'CP_5' | 'CP_3'
export type CarparkAvailabilityCode = 'ALL_CARPARKS' | 'TYPE2_CARPARKS'
export type CollectionLimitCode = 'MAX_RANGE'

export type PreferencesSettingCode =
  | MapDisplayCode
  | RoutePreferenceCode
  | ShowDemoCode
  | CarparkDistanceCode
  | CarparkCountCode
  | CarparkAvailabilityCode

export type PreferenceSettingMasterList = {
  MapDisplay: PreferenceSettingObject<MapDisplayCode>[]
  RoutePreference: PreferenceSettingObject<RoutePreferenceCode>[]
  ShowDemo: PreferenceSettingObject<ShowDemoCode>[]
  CarparkDistance: PreferenceSettingObject<CarparkDistanceCode>[]
  CarparkCount: PreferenceSettingObject<CarparkCountCode>[]
  CarparkAvailability: PreferenceSettingObject<CarparkAvailabilityCode>[]
  CollectionLimit: PreferenceSettingObject<CollectionLimitCode>[]
  BookmarkLimit: PreferenceSettingObject<CollectionLimitCode>[]
  ShowSavedCarparkListView: PreferenceSettingObject<ShowSavedCarparkListViewCode>[]
  vehicleTypes: PreferenceSettingObject<VehicleType>[]
  vehiclePowerTypes: PreferenceSettingObject<PowerType>[]
  vehicleFuelGrade: PreferenceSettingObject<FuelGradeType>[]
  TrafficInformation: PreferenceSettingObject<YesNoType>[]
  ParkingAvailabilityDisplay: PreferenceSettingObject<YesNoType>[]
  ParkingAvailabilityAudio: PreferenceSettingObject<YesNoType>[]
  EstimatedTravelTimeDisplay: PreferenceSettingObject<YesNoType>[]
  EstimatedTravelTimeAudio: PreferenceSettingObject<YesNoType>[]
}

export type PreferenceSettingMasterListResponse = {
  masterlists: PreferenceSettingMasterList
}

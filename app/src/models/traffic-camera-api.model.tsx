export interface TrafficCamerasRequest {
  latitude: number
  longitude: number
  zone?: string
  radius?: number
}

export interface TrafficCameraInfo {
  id: string
  cameraID: number
  name: string
  lat: number
  long: number
  tag: string
  orderSequence: number
  distance: number
  imageLink: string
  lastImageTime?: number
}

export interface ExpressWay {
  code: string
  value: string
  module: string
  default?: string
}

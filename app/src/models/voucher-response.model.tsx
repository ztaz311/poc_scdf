import { ImageStyle, TextStyle } from 'react-native'

export type VoucherRedeemResponseData = {
  type: 'TEXT' | 'IMAGE' | 'LIST'
  style: {
    dark: TextStyle | ImageStyle
    light: TextStyle | ImageStyle
  }
  items: VoucherRedeemResponseItems[]
  readmoreLines: number
}
export type VoucherRedeemResponseItems = {
  text: string
  image: {
    light: string
    dark: string
  }
}
type VoucherRedeemButton = {
  button_text: string
  button_visible: boolean
}
export type VoucherRedeemDetails = {
  data: VoucherRedeemResponseData[]
  cta_ok?: VoucherRedeemButton
  cta_cancel?: VoucherRedeemButton
}
export type ActivityCountType = {
  performedActivity: number
  totalActivity: number
}

export type ParagraphContentType = {
  header: string
  list: string[]
}

export type InfoSectionType = {
  title: string
  paragraph: ParagraphContentType[]
  readmoreLines: number
}

export type MoreInfo = {
  section: InfoSectionType[]
  title: string
  description: string
  footer: string
  footerDetails: {
    type: 'FEEDBACK' | 'CONTACT_US'
    title: string
    description: string
  }
}

export type RedeemAmountType = {
  amount: number
  date: number
}

export type UtilizeAmountType = {
  amount: number
  date: number
  location: string
}

export type VoucherType = {
  updatedOn?: string | number | Date
  activityCount: ActivityCountType
  description: string
  expireDate: number
  moreInfoContent: MoreInfo
  operatorName: string
  carparkName?: string
  status: VoucherStatusEnum
  title: string
  totalAmount: number
  voucherId: string
  seen?: boolean
  selected?: boolean
  carParkId?: string[]
  redeemExpiryDays?: number
  redeemExpiryInDays?: number
  redeemedAmount?: RedeemAmountType[]
  utilizedAmount?: UtilizeAmountType[]
  redeemedOn?: number
  expiredOn?: number
  carparkIds?: string[]
  transactionTimeStamp?: number
  location: string
  address: string
  operatorType: 'GENERAL' | 'FIXED'
  shouldValidateIU: boolean
  voucherPriceDetails?: {
    image?: string
    description?: string
  }
  redeemWarningMessage?: string
  validationDetails: VoucherRedeemDetails
  redeemDetails: VoucherRedeemDetails
  voucherType: 'REDEEM_GENERAL' | 'REDEEM_COPY' | 'REDEEM_QR' | 'REDEEM_PARKING' | 'REDEEM_LINK'
  redeemCode?: string
  redeemLink?: string
}

export enum VoucherStatusEnum {
  ReadyToRedeem = 'ready_to_redeem',
  Pending = 'pending',
  Redeemed = 'redeemed',
  Utilized = 'utilized',
  Expired = 'expired',
  FullyClaimed = 'fully_claimed',
}

export type VoucherOperatorType = {
  carparkId: string
  carparkName: string
  operatorName: string
}

export type TransactionType = {
  operatorName: string
  amount: number
  date: number
  location: string
  title: string
}

export type VoucherResponse = {
  transactions: TransactionType[]
  vouchers: VoucherType[]
}

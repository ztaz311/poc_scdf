import { ParkingTypesEnum } from '../enums/parking-types.enum'

export type ParkingDay =
  | 'MONDAY'
  | 'TUESDAY'
  | 'WEDNESDAY'
  | 'THURSDAY'
  | 'FRIDAY'
  | 'SATURDAY'
  | 'SUNDAY'
  | 'PUBLIC_HOLIDAY'

export enum ParkingPriceValue {
  NO_INFO = 'No Info',
  SEASON = 'Season',
  CUSTOMER_ONLY = 'Customers Only',
  FREE = 'Free',
}

export interface CalculatePriceRequest {
  latitude?: string
  longitude?: string
  radius?: string
  startDateTime?: string
  endDateTime?: string
  id?: string
  dayofweek?: ParkingDay
}

export interface SeasonParkingTime {
  startTime: string
  endTime: string
}

export interface CarparkCalculatedRate {
  id: string
  name: string
  lat: number
  long: number
  source: string
  distance: number
  address: string
  parkingRate: number | 'No Info' | 'Season' | 'Customers Only' | 'Free'
  isCheapest: boolean
  remarks: string[]
  seasonParking?: SeasonParkingTime[] | null
  customerParking?: SeasonParkingTime[] | null
  parkingType: ParkingTypesEnum
  hasVouchers?: boolean
  voucherAmount?: number
}

export interface CarparkCalculatedRateList {
  size: number
  value: CarparkCalculatedRate[]
}
export type CarparkList = {
  id: string
  name: string
  parkingSystem: string
  capacity: number
  category: string
  lat: number
  long: number
  source: string
  distance: number
  availablelot: number
  currentHrRate: {
    start_time: string
    end_time: string
    oneHrRate: number
  }
  nextHrRate: {
    start_time: string
    end_time: string
    oneHrRate: number
  }
  isCheapest: boolean
  destinationCarPark: boolean
  parkingType: string
  hasRatesInfo: boolean
  availablePercent: number
  availablePercentImageBand: string
  hasVouchers: boolean
  voucherAmount: number
  isBookmarked: boolean
}

export type FindCarpark = {
  arrivalTime: number
  destName: string
  isVoucher: false
  lat: number
  lng: number
  amenities: [
    {
      filter: {
        availableBand: string
      }
      type: string
    },
  ]
  maxRad: number
  rad: number
  resultcount: number
}

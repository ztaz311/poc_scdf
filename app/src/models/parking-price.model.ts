import { ParkingTypesEnum } from '../enums/parking-types.enum'
import { CarparkAvailabilities, CarparkImages, SubAmenity } from './amenity-details.model'
import { RemoteTextStyle, RemoteViewStyle } from './remote-style.model'

export type RemoteText = {
  value: string
  style?: RemoteTextStyle
}

export type ParkingRateCell = {
  style?: RemoteViewStyle
  title: RemoteText
  description?: RemoteText
  remarks?: RemoteText[]
}

export type ParkingRateRow = {
  remarks?: RemoteText[]
  cells: ParkingRateCell[]
  style?: RemoteViewStyle
}

export type ParkingRateTable = {
  timeRange: string
  rateId: number
  remarks?: RemoteText[]
  rows: ParkingRateRow[]
  default: boolean
}

export type ParkingPriceDetails = {
  id: string
  name: string
  parkingType: ParkingTypesEnum
  rates: ParkingRateTable[]
  mtcRates: ParkingRateTable[]
  lbRates: ParkingRateTable[]
  amenities: SubAmenity[]
  images: CarparkImages
  availability: CarparkAvailabilities
  remarks: { value: string }[]

  parkingRatesInfo: string
}

export type ParkingPriceDetailsErrorResponse = {
  message?: string
}

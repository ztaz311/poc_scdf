export type Location = {
  lat: string
  long: string
}

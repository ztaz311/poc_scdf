export enum UserOnboardingState {
  ONBOARDING_INIT = 'ONBOARDING_INIT',
  ONBOARDING_INTRO = 'ONBOARDING_INTRO',
  ONBOARDING_STEP_1 = 'ONBOARDING_STEP_1',
  ONBOARDING_COMPLETED = 'ONBOARDING_COMPLETED',
}

export class UserOnboardingStateData {
  userOnboardingState?: UserOnboardingState | null
}

export enum SubICategoryDisplayType {
  TILE = 'TILE',
  FLAT = 'FLAT',
}

export interface UserMapObjectPreferenceBase {
  order: number
  font_type?: string | null
  element_id: string
  button_type?: string | null
  display_text?: string
  display_type?: string
  element_name?: string
  map_icon_url?: string | null
  map_icon_light_selected_url?: string
  map_icon_light_unselected_url?: string
  map_icon_dark_selected_url?: string
  map_icon_dark_unselected_url?: string
  button_text_color?: string
  tutorial_image_url?: string | null
  button_active_color?: string
  button_inactive_color?: string
  button_active_image_url?: string
  button_inactive_image_url?: string
  landing_amenities_url?: string | null
  landing_amenities_light_selected_url?: string
  landing_amenities_light_unselected_url?: string
  landing_amenities_dark_selected_url?: string
  landing_amenities_dark_unselected_url?: string
  is_selected: boolean
  is_updatable: boolean
  display_onboarding: boolean
  display_bottomsheet: boolean
  isFeatureUsed: boolean
}

export interface UserMapObjectItemPreference extends UserMapObjectPreferenceBase {
  sub_items: UserMapObjectPreferenceBase[]
  tutorial_image_cordinate_x: string
  tutorial_image_cordinate_y: string
}

export interface Profile {
  element_id: string
  element_name: string
  display_text: string
  landing_amenities_light_selected_url?: string
  landing_amenities_light_unselected_url?: string
  landing_amenities_dark_selected_url?: string
  landing_amenities_dark_unselected_url?: string
  zones: {
    center_point: string
    radius: number
    zone_id: string
    active_zoom_level: number
    deactive_zoom_level: number
  }[]
  amenities: string[]
  default_amenities: {
    type: string
    ids: string[]
  }[]
  season_parking: {
    lat: number
    long: number
  }[]
  isFeatureUsed: boolean
  showAsAmenity: boolean
}

export interface UserPreference {
  notification_preference: UserMapObjectItemPreference[]
  amenities_preference: UserMapObjectItemPreference[]
  profiles?: Profile[] // todo: make this non-optional later?
}

export interface UserPreferenceUpdate extends UserPreference {
  onboarding_state?: UserOnboardingState | null
}

export interface UserPreferenceMessage extends UserPreference {
  preference_id: number
  user_id: number
  create_time: string
  update_time?: string | null
}

export enum UserPreferenceCacheBurstStrategy {
  FETCH_UNINTIALIZED,
  FETCH_ALWAYS,
}

export interface LocalUserData {
  cognitoID: string
  carplateNumber?: string
  iuNumber?: string
  userName: string
  email: string
}

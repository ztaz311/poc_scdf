export type FavouriteLocationApiModel = {
  addressid?: number
  lat: string
  long: string
  name: string
  address1: string
  address2: string
}

export type FavListGetResponse = {
  address: FavouriteLocationApiModel[]
}

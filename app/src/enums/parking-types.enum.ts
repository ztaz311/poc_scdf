export enum ParkingTypesEnum {
  ShortTermParking = 'SHORT_TERM_PARKING',
  SeasonParking = 'SEASON_PARKING',
  PartialSeasonParking = 'PARTIAL_SEASON_PARKING',
  CustomerParking = 'CUSTOMER_PARKING',
}

export enum ParkingDetailRate {
  Car = 'CAR',
  Motorcycle = 'MOTORCYCLE',
  LoadingBay = 'LOADING_BAY',
}

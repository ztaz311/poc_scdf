export enum LandingModesEnum {
  NearbyAmenities = 'nearby_amenities',
  LiveCamera = 'live_camera',
}

export enum LocationTypesEnum {
  Normal = 'Normal',
  Current = 'Current',
  Destination = 'Destination',
}

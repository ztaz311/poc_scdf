export enum BottomSheetIndexesEnum {
  MIN = 0,
  HALF = 1,
  MAX = 2,
}

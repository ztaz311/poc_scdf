export enum DayTypesEnum {
  WeekDay = 'Weekdays',
  Saturday = 'Saturday',
  Sunday = 'Sunday',
}

export enum DaysEnum {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
}

export enum SearchFromScreensEnum {
  Landing = 'Landing',
  RoutePlanning = 'RoutePlanning',
  ETA = 'ETA',
}

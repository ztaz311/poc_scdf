import { useEffect } from 'react'
import EventEmitter from '../utils/events'

export function useEventEmitter(eventName: string, handler: Function) {
  useEffect(() => {
    const listener = EventEmitter.addEventListener(eventName, handler)
    return () => {
      EventEmitter.removeListener(eventName, listener)
    }
  }, [])
}

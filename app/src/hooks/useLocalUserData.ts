import { useEffect, useState } from 'react'
import { LocalUserData } from '../models/user-preference-api.model'
import UserService from '../services/user.service'

export const useLocalUserData = () => {
  const [localUserData, setLocalUserData] = useState<LocalUserData>({
    cognitoID: '',
    carplateNumber: '',
    iuNumber: '',
    userName: '',
    email: '',
  })

  useEffect(() => {
    getLocalUserData()
    return () => {
      setLocalUserData({
        cognitoID: '',
        carplateNumber: '',
        iuNumber: '',
        userName: '',
        email: '',
      })
    }
  }, [])

  const getLocalUserData = async () => {
    const data = await UserService.getLocalUserData()
    setLocalUserData(data)
  }
  return localUserData
}

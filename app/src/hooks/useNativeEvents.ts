import { useEffect } from 'react'
import { DeviceEventEmitter, NativeEventEmitter, NativeModules, Platform } from 'react-native'
import ShareBusinessLogic, { EventData } from '../services/share-business-logic/share-business-logic.service'

export const useNativeEvents = (eventType: string, callback: (event: EventData) => void) => {
  useEffect(() => {
    const emitter =
      Platform.OS === 'ios' ? new NativeEventEmitter(NativeModules.ReactNativeEventEmitter) : DeviceEventEmitter
    const event = emitter.addListener(eventType, callback)
    return () => {
      event.remove()
    }
  }, [callback, eventType])
}

export const useShareBusinessLogic = (callback: (event: EventData) => void) =>
  useNativeEvents(ShareBusinessLogic.constants.SHARE_BUSINESS_LOGIC_REQUEST, callback)
